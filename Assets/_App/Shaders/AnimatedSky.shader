Shader "ArtPuzzle/AnimatedSky" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Main Texture", 2D) = "white" {}
        _Speed ("Speed", float) = 0.1
    }
    
    SubShader {
        Tags { "RenderType" = "Opaque" }
        LOD 200
        
        CGPROGRAM
        #pragma surface surf Standard

        sampler2D _MainTex;

        struct Input {
            float2 uv_MainTex;
        };

        fixed4 _Color;
        float _Speed;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o) {
            float2 uv = IN.uv_MainTex + float2(_Speed * _Time.y, 0);
            fixed4 c = tex2D (_MainTex, uv) * _Color * 2;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
    
    FallBack "Diffuse"
}