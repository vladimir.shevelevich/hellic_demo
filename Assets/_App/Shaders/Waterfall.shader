// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:False,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:34474,y:33056,varname:node_3138,prsc:2|emission-9308-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:33990,y:32923,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Clamp01,id:6653,x:33743,y:33451,varname:node_6653,prsc:2|IN-5657-OUT;n:type:ShaderForge.SFN_Tex2d,id:7779,x:33220,y:33459,varname:node_7779,prsc:2,tex:40cbf75ee360f7d44b739281932a7bd2,ntxv:0,isnm:False|UVIN-6308-OUT,TEX-8477-TEX;n:type:ShaderForge.SFN_RemapRange,id:5657,x:33582,y:33451,varname:node_5657,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:2|IN-2974-OUT;n:type:ShaderForge.SFN_Multiply,id:4734,x:33990,y:33291,varname:node_4734,prsc:2|A-1433-OUT,B-6653-OUT;n:type:ShaderForge.SFN_TexCoord,id:4532,x:31328,y:33259,varname:node_4532,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:3786,x:32134,y:33981,ptovrint:False,ptlb:U_OffsetMainWave,ptin:_U_OffsetMainWave,varname:_u_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:4111,x:32134,y:34082,ptovrint:False,ptlb:V_OffsetMainWave,ptin:_V_OffsetMainWave,varname:_v_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Append,id:1527,x:32322,y:33957,varname:node_1527,prsc:2|A-3786-OUT,B-4111-OUT;n:type:ShaderForge.SFN_Multiply,id:3053,x:32491,y:33957,varname:node_3053,prsc:2|A-1527-OUT,B-1156-T;n:type:ShaderForge.SFN_Add,id:6087,x:32848,y:33957,varname:node_6087,prsc:2|A-1121-OUT,B-4532-UVOUT;n:type:ShaderForge.SFN_Time,id:1156,x:32321,y:34124,varname:node_1156,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:9199,x:33213,y:32997,varname:node_9199,prsc:2,tex:40cbf75ee360f7d44b739281932a7bd2,ntxv:0,isnm:False|UVIN-205-OUT,TEX-8477-TEX;n:type:ShaderForge.SFN_Append,id:1021,x:32190,y:32767,varname:node_1021,prsc:2|A-4868-OUT,B-8881-OUT;n:type:ShaderForge.SFN_Multiply,id:5611,x:32420,y:32767,varname:node_5611,prsc:2|A-1021-OUT,B-845-T;n:type:ShaderForge.SFN_Add,id:5650,x:32788,y:32767,varname:node_5650,prsc:2|A-3653-OUT,B-4532-UVOUT;n:type:ShaderForge.SFN_Time,id:845,x:32190,y:32923,varname:node_845,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:4868,x:31996,y:32767,ptovrint:False,ptlb:U_OffsetWave01,ptin:_U_OffsetWave01,varname:node_4868,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:8881,x:31996,y:32855,ptovrint:False,ptlb:V_OffsetWave01,ptin:_V_OffsetWave01,varname:node_8881,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:9907,x:33990,y:33117,varname:node_9907,prsc:2|A-8380-OUT,B-9155-OUT;n:type:ShaderForge.SFN_Slider,id:9155,x:33504,y:33215,ptovrint:False,ptlb:IntansityWave01,ptin:_IntansityWave01,varname:node_9155,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2321475,max:1;n:type:ShaderForge.SFN_ComponentMask,id:7664,x:33407,y:32997,varname:node_7664,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9199-R;n:type:ShaderForge.SFN_ComponentMask,id:2974,x:33405,y:33459,varname:node_2974,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7779-B;n:type:ShaderForge.SFN_Add,id:9308,x:34242,y:33142,varname:node_9308,prsc:2|A-7241-RGB,B-9907-OUT,C-4734-OUT;n:type:ShaderForge.SFN_Frac,id:3653,x:32594,y:32767,varname:node_3653,prsc:2|IN-5611-OUT;n:type:ShaderForge.SFN_Frac,id:1121,x:32653,y:33957,varname:node_1121,prsc:2|IN-3053-OUT;n:type:ShaderForge.SFN_Slider,id:1433,x:33504,y:33332,ptovrint:False,ptlb:IntensityMainWave,ptin:_IntensityMainWave,varname:node_1433,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6116505,max:1;n:type:ShaderForge.SFN_Clamp01,id:8380,x:33728,y:32997,varname:node_8380,prsc:2|IN-1297-OUT;n:type:ShaderForge.SFN_RemapRange,id:1297,x:33568,y:32997,varname:node_1297,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:2|IN-7664-OUT;n:type:ShaderForge.SFN_Append,id:9716,x:31374,y:32342,varname:node_9716,prsc:2|A-6121-OUT,B-1706-OUT;n:type:ShaderForge.SFN_Multiply,id:7244,x:31570,y:32342,varname:node_7244,prsc:2|A-9716-OUT,B-6743-T;n:type:ShaderForge.SFN_Time,id:6743,x:31374,y:32498,varname:node_6743,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:6121,x:31211,y:32369,ptovrint:False,ptlb:U_OffsetDistortion02,ptin:_U_OffsetDistortion02,varname:_U_OffsetWave02,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_ValueProperty,id:1706,x:31211,y:32520,ptovrint:False,ptlb:V_OffsetDistortion02,ptin:_V_OffsetDistortion02,varname:_V_OffsetWave02,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Frac,id:4279,x:31728,y:32342,varname:node_4279,prsc:2|IN-7244-OUT;n:type:ShaderForge.SFN_Add,id:205,x:32987,y:32725,varname:node_205,prsc:2|A-3206-OUT,B-5650-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9035,x:32256,y:32461,varname:node_9035,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5889-G;n:type:ShaderForge.SFN_Clamp01,id:3332,x:32586,y:32461,varname:node_3332,prsc:2|IN-3605-OUT;n:type:ShaderForge.SFN_RemapRange,id:3605,x:32429,y:32461,varname:node_3605,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:2|IN-9035-OUT;n:type:ShaderForge.SFN_Multiply,id:3206,x:32786,y:32461,varname:node_3206,prsc:2|A-3332-OUT,B-2327-OUT;n:type:ShaderForge.SFN_Tex2d,id:5889,x:32080,y:32461,varname:node_5889,prsc:2,tex:40cbf75ee360f7d44b739281932a7bd2,ntxv:0,isnm:False|UVIN-2525-OUT,TEX-8477-TEX;n:type:ShaderForge.SFN_Add,id:2525,x:31898,y:32342,varname:node_2525,prsc:2|A-4279-OUT,B-4532-UVOUT;n:type:ShaderForge.SFN_Add,id:6308,x:33025,y:33574,varname:node_6308,prsc:2|A-3853-OUT,B-6087-OUT;n:type:ShaderForge.SFN_Append,id:9377,x:31418,y:33629,varname:node_9377,prsc:2|A-3212-OUT,B-8033-OUT;n:type:ShaderForge.SFN_Multiply,id:6923,x:31607,y:33629,varname:node_6923,prsc:2|A-9377-OUT,B-6709-T;n:type:ShaderForge.SFN_Time,id:6709,x:31418,y:33798,varname:node_6709,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:3212,x:31201,y:33644,ptovrint:False,ptlb:U_OffsetDistortion01,ptin:_U_OffsetDistortion01,varname:_U_OffsetWave03,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:8033,x:31201,y:33730,ptovrint:False,ptlb:V_OffsetDistortion01,ptin:_V_OffsetDistortion01,varname:_V_OffsetWave03,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Frac,id:6835,x:31770,y:33629,varname:node_6835,prsc:2|IN-6923-OUT;n:type:ShaderForge.SFN_Add,id:195,x:31958,y:33629,varname:node_195,prsc:2|A-6835-OUT,B-4532-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3697,x:32146,y:33629,varname:node_3697,prsc:2,tex:40cbf75ee360f7d44b739281932a7bd2,ntxv:0,isnm:False|UVIN-195-OUT,TEX-8477-TEX;n:type:ShaderForge.SFN_ComponentMask,id:4409,x:32314,y:33629,varname:node_4409,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-3697-G;n:type:ShaderForge.SFN_Clamp01,id:747,x:32646,y:33629,varname:node_747,prsc:2|IN-2566-OUT;n:type:ShaderForge.SFN_RemapRange,id:2566,x:32489,y:33629,varname:node_2566,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:2|IN-4409-OUT;n:type:ShaderForge.SFN_Multiply,id:3853,x:32847,y:33629,varname:node_3853,prsc:2|A-747-OUT,B-2194-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:8477,x:31328,y:33042,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_8477,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:40cbf75ee360f7d44b739281932a7bd2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:2194,x:32489,y:33811,ptovrint:False,ptlb:IntensityDistirtion01,ptin:_IntensityDistirtion01,varname:node_2194,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:2327,x:32429,y:32641,ptovrint:False,ptlb:IntensityDistortiob02,ptin:_IntensityDistortiob02,varname:node_2327,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:8477-7241-3786-4111-1433-4868-8881-9155-3212-8033-2194-6121-1706-2327;pass:END;sub:END;*/

Shader "GarbuzShader/Water" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _U_OffsetMainWave ("U_OffsetMainWave", Float ) = 0.1
        _V_OffsetMainWave ("V_OffsetMainWave", Float ) = 0.1
        _IntensityMainWave ("IntensityMainWave", Range(0, 1)) = 0.6116505
        _U_OffsetWave01 ("U_OffsetWave01", Float ) = 0.1
        _V_OffsetWave01 ("V_OffsetWave01", Float ) = 0.1
        _IntansityWave01 ("IntansityWave01", Range(0, 1)) = 0.2321475
        _U_OffsetDistortion01 ("U_OffsetDistortion01", Float ) = 0.1
        _V_OffsetDistortion01 ("V_OffsetDistortion01", Float ) = 0.1
        _IntensityDistirtion01 ("IntensityDistirtion01", Range(0, 1)) = 0
        _U_OffsetDistortion02 ("U_OffsetDistortion02", Float ) = 0.2
        _V_OffsetDistortion02 ("V_OffsetDistortion02", Float ) = 0.2
        _IntensityDistortiob02 ("IntensityDistortiob02", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _U_OffsetMainWave;
            uniform float _V_OffsetMainWave;
            uniform float _U_OffsetWave01;
            uniform float _V_OffsetWave01;
            uniform float _IntansityWave01;
            uniform float _IntensityMainWave;
            uniform float _U_OffsetDistortion02;
            uniform float _V_OffsetDistortion02;
            uniform float _U_OffsetDistortion01;
            uniform float _V_OffsetDistortion01;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _IntensityDistirtion01;
            uniform float _IntensityDistortiob02;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_6743 = _Time;
                float2 node_4279 = frac((float2(_U_OffsetDistortion02,_V_OffsetDistortion02)*node_6743.g));
                float2 node_2525 = (node_4279+i.uv0);
                float4 node_5889 = tex2D(_MainTex,TRANSFORM_TEX(node_2525, _MainTex));
                float4 node_845 = _Time;
                float2 node_205 = ((saturate((node_5889.g.r*3.0+-1.0))*_IntensityDistortiob02)+(frac((float2(_U_OffsetWave01,_V_OffsetWave01)*node_845.g))+i.uv0));
                float4 node_9199 = tex2D(_MainTex,TRANSFORM_TEX(node_205, _MainTex));
                float4 node_6709 = _Time;
                float2 node_195 = (frac((float2(_U_OffsetDistortion01,_V_OffsetDistortion01)*node_6709.g))+i.uv0);
                float4 node_3697 = tex2D(_MainTex,TRANSFORM_TEX(node_195, _MainTex));
                float4 node_1156 = _Time;
                float2 node_6308 = ((saturate((node_3697.g.r*3.0+-1.0))*_IntensityDistirtion01)+(frac((float2(_U_OffsetMainWave,_V_OffsetMainWave)*node_1156.g))+i.uv0));
                float4 node_7779 = tex2D(_MainTex,TRANSFORM_TEX(node_6308, _MainTex));
                float3 emissive = (_Color.rgb+(saturate((node_9199.r.r*3.0+-1.0))*_IntansityWave01)+(_IntensityMainWave*saturate((node_7779.b.r*3.0+-1.0))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
