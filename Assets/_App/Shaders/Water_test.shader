﻿Shader "Playgendary/Water_test"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MainTexTint("Main Tex Tint", Color) = (1,1,1,1)
        _NoiseTex("Noise", 2D) = "white" {}
        _NoiseCutOff ("Noise CutOff", Range(0,1)) = 0.75
        _FoamDistance("Foam Distance", Range (0,1))= 0.4
        _NoiseColor ("Noise Color", Color) = (1,1,1,1)
        _ScrollX ("Scroll X", float) = 0
        _ScrollY ("Scroll Y", float) = 0
        _OverrideColor("Override Color", Color) = (1,1,1,0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque"  }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;
            float _NoiseCutOff;
            float _FoamDistance;
            sampler2D _CameraDepthTexture;
            fixed4 _NoiseColor;
            float _ScrollX;
            float _ScrollY;
            sampler2D _CameraNormalsTexture;
            fixed4 _MainTexTint;
            fixed4 _OverrideColor;

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 noiseuv : TEXCOORD1;
                float2 noiseuv_static : TEXCOORD3;
                
                float4 vertex : SV_POSITION;
                float4 screenPosition : TEXCOORD2;
                float depth : SV_Depth;
                float3 viewNormal : NORMAL;
            };

            

            v2f vert (appdata v)
            {
                v2f o;
                _ScrollX *= _Time;
                _ScrollY *= _Time;
                float2 new_uv = v.uv + float2(_ScrollX, _ScrollY); 
                float2 new_uv2 = v.uv + float2(_ScrollX/2, _ScrollY/2);
                o.vertex = UnityObjectToClipPos(v.vertex);
                
                o.screenPosition = ComputeScreenPos(o.vertex); //Calculating screenPos
                
                o.uv = TRANSFORM_TEX(new_uv, _MainTex);
                o.noiseuv = TRANSFORM_TEX(new_uv2, _NoiseTex);
                o.noiseuv_static = TRANSFORM_TEX(v.uv, _NoiseTex); // это для блендинга бликов на воде
                //o.viewNormal = COMPUTE_VIEW_NORMAL;
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                
                float existingDepth01 = tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPosition)).r; 
                
                //float existingDepth01 = 0;
                float existingDepthLinear = LinearEyeDepth(existingDepth01);
                float depthDifference = existingDepthLinear - i.screenPosition.w;

                float foamDepthDifference01 = saturate(depthDifference / _FoamDistance);
                
                fixed4 water = tex2D(_MainTex, i.uv) * _MainTexTint;
                fixed noise = tex2D(_NoiseTex, i.noiseuv).r * (tex2D(_NoiseTex, i.noiseuv_static).r * 2) ;
               
                float foamNoiseCutOff = foamDepthDifference01 * _NoiseCutOff;
                fixed noise_cutoff = noise > foamNoiseCutOff ? _NoiseColor : 0;
               
                fixed4 col = (water + noise_cutoff);
                col.rgb = lerp(col.rgb, _OverrideColor.rgb, _OverrideColor.a);

                return col;
            }
            ENDCG
        }
    }
}

// Принцип работы взят отсюда https://roystan.net/articles/toon-water.html
