using System;
using App.GameData;
using App.Game;
using UnityEngine;

namespace App
{
    public class EntryPoint : MonoBehaviour
    {
        [SerializeField] private RectTransform _uiRoot;
        [SerializeField] private ContentProvider contentProvider;

        private IDisposable _gameEntity;

        private void Start()
        {
            CreateGameEntity();
        }

        private void CreateGameEntity()
        {
            var ctx = new GameEntity.Ctx
            {
                UiRoot = _uiRoot,
                ContentProvider = contentProvider
            };
            _gameEntity = new GameEntity(ctx);
        }

        private void OnDestroy()
        {
            _gameEntity?.Dispose();
        }
    }
}
