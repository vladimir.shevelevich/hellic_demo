﻿using System;
using App.Game.Enums;
using App.Game.Level;
using App.GameData.Settings;
using GameData;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace App.GameData
{
    [CreateAssetMenu(fileName = "ContentProvider", menuName = "GameData/ContentProvider", order = 0)]
    public class ContentProvider : ScriptableObject
    {
        public GameObject PlayerPrefabRef;
        public GameObject InputReaderPrefabRef;
        public GameObject LevelCameraPrefabRef;
        public GameObject UpgradeSceneCameraPrefabRef;
        public GameObject FinishTriggerRef;
        public GameObject LevelCompleteScreenRef;
        public GameObject LevelRestartScreenRef;
        public GameObject LoadingScreenRef;
        public GameObject LevelHudRef;
        public GameObject TutorialScreenRef;
        public GameObject UpgradeScreenRef;
        public GameObject UpgradeSceneEnvironmentPrefabRef;
        public GameObject BuildingRef;
        public GameObject SmallMoneyKeeperRef;
        public GameObject BigMoneyKeeperRef;

        public GameObject RocketAimPrefab;
        public GameObject PlayerRocketPrefab;
        public GameObject EnemyRocketPrefab;
        public GameObject GunBulletPrefab;
        public GameObject DamageNumberPrefab;
        public GameObject PopupNotificationPrefab;
        public LevelConfig[] LevelConfigs;

        public EnemyContent Enemies;
        public VfxContent Vfx;
        public SettingsContent Settings;

        [Serializable]
        public class SettingsContent
        {
            public PlayerSettings PlayerSettings;
            public AttackPrioritySettings AttackPrioritySettings;
        }

        [Serializable]
        public class EnemyContent
        {
            [SerializeField] private GameObject FootmanEnemyPrefabRef;
            [SerializeField] private GameObject JeepEnemyPrefabRef;
            [SerializeField] private GameObject TankEnemyPrefabRef;
            [SerializeField] private GameObject TowerEnemyPrefabRef;
            [SerializeField] private GameObject BarrelPropsPrefabRef;
            [SerializeField] private GameObject TowerPropsPrefabRef;
            [SerializeField] private GameObject SmallBuildingPropsPrefabRef;
            [SerializeField] private GameObject MediumBuildingPropsPrefabRef;

            public GameObject PrefabRefByType(EnemyType enemyType)
            {
                switch (enemyType)
                {
                    case EnemyType.Footman:
                        return FootmanEnemyPrefabRef;
                    case EnemyType.Jeep:
                        return JeepEnemyPrefabRef;
                    case EnemyType.Tank:
                        return TankEnemyPrefabRef;
                    case EnemyType.Tower:
                        return TowerEnemyPrefabRef;
                    case EnemyType.BarrelProps:
                        return BarrelPropsPrefabRef;
                    case EnemyType.TowerProps:
                        return TowerPropsPrefabRef;
                    case EnemyType.SmallBuildingProps:
                        return SmallBuildingPropsPrefabRef;
                    case EnemyType.MediumBuildingProps:
                        return MediumBuildingPropsPrefabRef;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(enemyType), enemyType, null);
                }
            }
        }

        [Serializable]
        public class VfxContent
        {
            public GameObject GunHitVfx;
            public GameObject RocketExplodeVfx;
        }
    }
}