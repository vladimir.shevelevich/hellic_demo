﻿using UnityEngine;

namespace App.GameData.Settings
{
    [CreateAssetMenu(fileName = "AttackPrioritySettings", menuName = "Settings/AttackPriority", order = 0)]
    public class AttackPrioritySettings : ScriptableObject
    {
        public int BarrelPriority;
        public int EnemyPriority;
        public int HostageBuildingPriority;
        public int MoneyKeeperPriority;
        public int DestroyablePropPriority;
    }
}