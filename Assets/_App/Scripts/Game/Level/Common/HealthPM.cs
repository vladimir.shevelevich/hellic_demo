﻿using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.Common
{
    public class HealthPM : BaseDisposable
    {
        public struct Ctx
        {
            public ReactiveProperty<float> CurrentHealth;
            public ReactiveProperty<bool> IsAlive;
            public IReadOnlyReactiveEvent<float> ApplyDamage;
            public ReactiveEvent<float> OnDamageReceived;
            public float StartingHealth;
            public IReadOnlyReactiveProperty<LevelState> LevelState;
        }

        private readonly Ctx _ctx;

        public HealthPM(Ctx ctx)
        {
            _ctx = ctx;
            AddForDisposal(_ctx.ApplyDamage.SubscribeWithSkip(ApplyDamage));

            _ctx.CurrentHealth.Value = _ctx.StartingHealth;
            _ctx.IsAlive.Value = true;
        }

        private void ApplyDamage(float damage)
        {
            if (!IsDamagePossible())
                return;

            _ctx.CurrentHealth.Value -= damage;
            _ctx.OnDamageReceived.Invoke(damage);
            
            if (_ctx.CurrentHealth.Value <= 0)
                Death();
        }

        private bool IsDamagePossible() => 
                _ctx.IsAlive.Value && _ctx.LevelState.Value == LevelState.InProcess;

        private void Death()
        {
            _ctx.IsAlive.Value = false;
        }
    }
}