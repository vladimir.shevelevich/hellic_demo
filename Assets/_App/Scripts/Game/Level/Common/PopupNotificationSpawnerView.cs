﻿using System;
using App.Game.Level.PopupNotification;
using App.GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace _App.Scripts.Game.Level.Common
{
    public class PopupNotificationSpawnerView : MonoBehaviour
    {
        private const float TimerBeforeDestroy = 2f;
        
        public struct Ctx
        {
            public GameObject PopupPrefab;
            public IReadOnlyReactiveEvent<string> ShowNotification;
        }
        
        [SerializeField] private Transform _spawnPoint;

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _ctx.ShowNotification.SubscribeWithSkip(Spawn).AddTo(this);
        }

        private void Spawn(string text)
        {
            var ctx = new PopupNotificationEntity.Ctx
            {
                Text = text,
                Prefab = _ctx.PopupPrefab,
                SpawnPoint = _spawnPoint.position
            };
            
            var entity = new PopupNotificationEntity(ctx).AddTo(this);
            
            Observable.Timer(TimeSpan.FromSeconds(TimerBeforeDestroy))
                .Subscribe(_ => entity.Dispose())
                .AddTo(this);
        }
    }
}