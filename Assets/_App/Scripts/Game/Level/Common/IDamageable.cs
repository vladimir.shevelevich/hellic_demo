﻿using App.Game.Enums;
using UnityEngine;

namespace App.Game.Level.Common
{
    public interface IDamageable
    {
        public void ApplyDamage(float damage);
        public bool IsAlive { get; }
        public int AttackPriority { get; }
        public Transform TargetTransform { get; }
    }
}