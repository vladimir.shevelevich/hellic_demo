﻿using System;
using App.GameData;
using App.Scripts.Game.Level.DamageNumbers;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.Common
{
    public class DamageNumberSpawnerView : MonoBehaviour
    {
        private const float TimerBeforeDestroy = 2f;

        public struct Ctx
        {
            public ContentProvider ContentProvider;
            public IReadOnlyReactiveEvent<float> SpawnDamageNumber;
        }

        [SerializeField] private Transform _spawnPoint;

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _ctx.SpawnDamageNumber.SubscribeWithSkip(Spawn).AddTo(this);
        }

        private void Spawn(float damageValue)
        {
            var damageRound = Mathf.RoundToInt(damageValue);
            var ctx = new DamageNumberEntity.Ctx
            {
                Damage = damageRound,
                Prefab = _ctx.ContentProvider.DamageNumberPrefab,
                SpawnPoint = _spawnPoint.position
            };
            
            var entity = new DamageNumberEntity(ctx).AddTo(this);
            
            Observable.Timer(TimeSpan.FromSeconds(TimerBeforeDestroy))
                .Subscribe(_ => entity.Dispose())
                .AddTo(this);
        }
    }
}