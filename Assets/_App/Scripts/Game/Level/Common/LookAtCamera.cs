using UnityEngine;

namespace App.Scripts.Game.Level.Common
{
    public class LookAtCamera : MonoBehaviour
    {
        private UnityEngine.Camera _camera;
        private Transform _transform;
        private Transform _cameraTransform;
        
        private void Start()
        {
            _transform = transform;
            _cameraTransform = UnityEngine.Camera.main.transform;
        }

        private void LateUpdate()
        {
            var rotation = _cameraTransform.rotation;
            _transform.LookAt(_transform.position + rotation * Vector3.forward, rotation * Vector3.up);
        }
    }
}
