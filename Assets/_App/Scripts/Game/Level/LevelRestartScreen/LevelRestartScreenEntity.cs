﻿using System.Threading.Tasks;
using App.GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.LevelRestartScreen
{
    public class LevelRestartScreenEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<bool> IsPlayerAlive;
            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
            public ReactiveTrigger OnRestartLevelButtonClicked;
        }

        private readonly Ctx _ctx;

        public LevelRestartScreenEntity(Ctx ctx)
        {
            _ctx = ctx;
        }
        
        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var asset = _ctx.ContentProvider.LevelRestartScreenRef;
            
            if (asset == null)
                return;

            var ctx = new LevelRestartScreenView.Ctx
            {
                IsPlayerAlive = _ctx.IsPlayerAlive,
                OnRestartLevelButtonClicked = _ctx.OnRestartLevelButtonClicked
            };

            AddForDestroy(Object.Instantiate(asset.GetComponent<LevelRestartScreenView>(), _ctx.UiRoot))
                .SetCtx(ctx);
        }
    }
}