﻿using System;
using Protopunk.Tools;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.Game.Level.LevelRestartScreen
{
    public class LevelRestartScreenView : MonoBehaviour
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<bool> IsPlayerAlive;
            public ReactiveTrigger OnRestartLevelButtonClicked;
        }

        [SerializeField] private Button _restartButton;

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;

            _ctx.IsPlayerAlive.Skip(1).Subscribe(isAlive =>
            {
                if (isAlive)
                    return;

                ShowWithDelay(1);

            }).AddTo(this);

            Hide();
        }

        private void OnEnable() =>
            _restartButton.onClick.AddListener(OnRestartClicked);

        private void OnDisable() =>
            _restartButton.onClick.RemoveListener(OnRestartClicked);

        private void OnRestartClicked()
        {
            Hide();
            _ctx.OnRestartLevelButtonClicked?.Invoke();
        }

        private void Hide() =>
            gameObject.SetActive(false);

        private void ShowWithDelay(float delay)
        {
            Observable.Timer(TimeSpan.FromSeconds(delay)).Subscribe(_ => gameObject.SetActive(true)).AddTo(this);
        }
    }
}