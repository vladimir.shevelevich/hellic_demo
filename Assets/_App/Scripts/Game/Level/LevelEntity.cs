﻿using App.Scripts.Game.Level.Enemy;
using App.Scripts.Game.Level.Camera;
using App.Scripts.Game.Level.Input;
using App.Game.Level.Enemy;
using App.Game.Level.Environment;
using App.Game.Level.FinishTrigger;
using App.Game.Level.LevelHud;
using App.Game.Level.LoadingScreen;
using App.Game.Level.Player;
using App.Game.Level.TutorialScreen;
using App.GameData;
using App.Scripts.Game.Level.LevelCompleteScreen;
using App.Scripts.Game.Level.LevelRestartScreen;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level
{
    public class LevelEntity : BaseDisposable
    {
        public struct Ctx
        {
            public RectTransform UiRoot;
            public ContentProvider ContentProvider;
            public LevelConfig LevelConfig;
            public ReactiveTrigger LevelCompleted;
            public ReactiveTrigger RestartLevel;
            public int LevelIndex;
            public ReactiveProperty<int> MoneyAmount;
        }

        private readonly Ctx _ctx;

        private readonly ReactiveProperty<Vector2> _joystickInput;
        private readonly ReactiveProperty<Transform> _playerTransform;
        private readonly ReactiveProperty<PlayerEntity> _playerEntity;
        private readonly ReactiveProperty<bool> _isPlayerAlive;
        private readonly ReactiveTrigger _playerFinished;
        private readonly ReactiveProperty<Transform> _playerCameraTarget;
        private readonly ReactiveTrigger _levelLoaded;
        private readonly ReactiveProperty<int> _rescuedHostages;
        private readonly ReactiveTrigger _onHostageRescued;
        private readonly ReactiveProperty<LevelState> _levelState;
        private readonly ReactiveTrigger _onNextLevelButtonClicked;
        private readonly ReactiveTrigger _onRestartLevelButtonClicked;
        private readonly ReactiveProperty<float> _playerRocketCooldown;
        private readonly ReactiveEvent<int> _onMoneyRewarded;
        private readonly ReactiveProperty<int> _rewardedMoneyOnLevel;

        public LevelEntity(Ctx ctx)
        {
            _ctx = ctx;
            _joystickInput = AddForDisposal(new ReactiveProperty<Vector2>());
            _playerTransform = AddForDisposal(new ReactiveProperty<Transform>());
            _playerEntity = AddForDisposal(new ReactiveProperty<PlayerEntity>());
            _isPlayerAlive = AddForDisposal(new ReactiveProperty<bool>());
            _playerFinished = AddForDisposal(new ReactiveTrigger());
            _playerCameraTarget = AddForDisposal(new ReactiveProperty<Transform>());
            _levelLoaded = AddForDisposal(new ReactiveTrigger());
            _rescuedHostages = AddForDisposal(new ReactiveProperty<int>());
            _onHostageRescued = AddForDisposal(new ReactiveTrigger());
            _levelState = AddForDisposal(new ReactiveProperty<LevelState>());
            _onNextLevelButtonClicked = AddForDisposal(new ReactiveTrigger());
            _onRestartLevelButtonClicked = AddForDisposal(new ReactiveTrigger());
            _playerRocketCooldown = AddForDisposal(new ReactiveProperty<float>());
            _onMoneyRewarded = AddForDisposal(new ReactiveEvent<int>());
            _rewardedMoneyOnLevel = AddForDisposal(new ReactiveProperty<int>());

            CreatePM();
            BuildLevelAsync();
        }

        private void CreatePM()
        {
            var ctx = new LevelPM.Ctx
            {
                IsPlayerAlive = _isPlayerAlive,
                OnPlayerFinished = _playerFinished,
                LevelIndex = _ctx.LevelIndex,
                OnHostageRescued = _onHostageRescued,
                SavedHostages = _rescuedHostages,
                LevelState = _levelState,
                LevelCompleted = _ctx.LevelCompleted,
                RestartLevel = _ctx.RestartLevel,
                OnNextLevelButtonClicked = _onNextLevelButtonClicked,
                OnRestartLevelButtonClicked = _onRestartLevelButtonClicked,
                OnMoneyRewarded = _onMoneyRewarded,
                MoneyAmount = _ctx.MoneyAmount,
                RewardedMoneyOnLevel = _rewardedMoneyOnLevel
            };
            AddForDisposal(new LevelPM(ctx));
        }

        private async void BuildLevelAsync()
        {
            await CreateLoadingScreen().InitializeAsync();
            await CreateCamera().InitializeAsync();
            await CreateEnvironment().InitializeAsync();
            await CreatePlayer().InitializeAsync();
            await CreateFinishTrigger().InitializeAsync();
            await CreateLevelHud().InitializeAsync();
            await CreateTutorialScreen().InitializeAsync();
            await CreateLevelCompleteScreen().InitializeAsync();
            await CreateLevelRestartScreen().InitializeAsync();
            foreach (var enemyConfig in _ctx.LevelConfig.Enemies)
                await CreateEnemy(enemyConfig).InitializeAsync();
            await CreateInputReader().InitializeAsync();
            _levelLoaded.Invoke();
        }

        private LevelHudEntity CreateLevelHud()
        {
            var ctx = new LevelHudEntity.Ctx
            {
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot,
                RescuedHostages = _rescuedHostages,
                LevelIndex = _ctx.LevelIndex,
                RocketCooldown = _playerRocketCooldown,
                MoneyAmount = _ctx.MoneyAmount
            };
            return AddForDisposal(new LevelHudEntity(ctx));
        }

        private TutorialScreenEntity CreateTutorialScreen()
        {
            var ctx = new TutorialScreenEntity.Ctx
            {
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot,
                OnLevelLoaded = _levelLoaded
            };
            return AddForDisposal(new TutorialScreenEntity(ctx));
        }

        private LoadingScreenEntity CreateLoadingScreen()
        {
            var ctx = new LoadingScreenEntity.Ctx()
            {
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot,
                LevelLoaded = _levelLoaded,
                OnRestartLevelButtonClicked = _onRestartLevelButtonClicked,
                OnNextLevelButtonClicked = _onNextLevelButtonClicked
            };
            return AddForDisposal(new LoadingScreenEntity(ctx));
        }

        private EnvironmentEntity CreateEnvironment()
        {
            var ctx = new EnvironmentEntity.Ctx()
            {
                EnvironmentRef = _ctx.LevelConfig.EnvironmentPrefabRef
            };
            return AddForDisposal(new EnvironmentEntity(ctx));
        }

        private LevelRestartScreenEntity CreateLevelRestartScreen()
        {
            var ctx = new LevelRestartScreenEntity.Ctx()
            {
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot,
                IsPlayerAlive = _isPlayerAlive,
                OnRestartLevelButtonClicked = _onRestartLevelButtonClicked
            };
            return AddForDisposal(new LevelRestartScreenEntity(ctx));
        }

        private LevelCompleteScreenEntity CreateLevelCompleteScreen()
        {
            var ctx = new LevelCompleteScreenEntity.Ctx()
            {
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot,
                PlayerFinished = _playerFinished,
                OnNextLevelButtonClicked = _onNextLevelButtonClicked,
                RewardedMoneyOnLevel = _rewardedMoneyOnLevel
            };
            return AddForDisposal(new LevelCompleteScreenEntity(ctx));
        }

        private FinishTriggerEntity CreateFinishTrigger()
        {
            var ctx = new FinishTriggerEntity.Ctx()
            {
                LevelFinished = _playerFinished,
                ContentProvider = _ctx.ContentProvider,
                FinishPosition = _ctx.LevelConfig.FinishTriggerConfig.Position,
                RescuedHostages = _rescuedHostages,
                LevelConfig = _ctx.LevelConfig
            };
            return AddForDisposal(new FinishTriggerEntity(ctx));
        }

        private LevelCameraEntity CreateCamera()
        {
            var ctx = new LevelCameraEntity.Ctx
            {
                PlayerCameraTarget = _playerCameraTarget,
                ContentProvider = _ctx.ContentProvider
            };
            return AddForDisposal(new LevelCameraEntity(ctx));
        }

        private PlayerEntity CreatePlayer()
        {
            var ctx = new PlayerEntity.Ctx
            {
                JoystickInput = _joystickInput,
                PlayerTransform = _playerTransform,
                PlayerCameraTarget = _playerCameraTarget,
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot,
                IsAlive = _isPlayerAlive,
                PlayerConfig = _ctx.LevelConfig.PlayerConfig,
                LevelState = _levelState,
                RocketCooldown = _playerRocketCooldown
            };
            return _playerEntity.Value = AddForDisposal(new PlayerEntity(ctx));
        }

        private EnemyEntity CreateEnemy(EnemyConfig enemyConfig)
        {
            var ctx = new EnemyEntity.Ctx
            {
                ContentProvider = _ctx.ContentProvider,
                EnemyConfig = enemyConfig,
                LevelState = _levelState
            };
            return AddForDisposal(new EnemyEntity(ctx));
        }

        private InputReaderEntity CreateInputReader()
        {
            var ctx = new InputReaderEntity.Ctx
            {
                ContentProvider = _ctx.ContentProvider,
                JoystickInput = _joystickInput,
                UiRoot = _ctx.UiRoot
            };
            return AddForDisposal(new InputReaderEntity(ctx));
        }
    }
}