﻿using Protopunk.Tools;
using UniRx;

namespace App.Scripts.Game.Level.HealthBar
{
    public class HealthBarPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<float> CurrentHealth;
            public ReactiveProperty<bool> Visible;
            public int StartingHealth;
            public ReactiveProperty<float> Fullness;
        }

        public HealthBarPM(Ctx ctx)
        {
            ctx.Visible.Value = true;
            AddForDisposal(ctx.CurrentHealth.Subscribe(health =>
            {
                ctx.Fullness.Value = health / ctx.StartingHealth;
                if (health <= 0)
                    ctx.Visible.Value = false;
            }));
        }
    }
}