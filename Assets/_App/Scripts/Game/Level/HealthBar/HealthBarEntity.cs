using Protopunk.Tools;
using UniRx;

namespace App.Scripts.Game.Level.HealthBar
{
    public class HealthBarEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<float> CurrentHealth;
            public int StartingHealth;
            public HealthBarView View;
        }

        private readonly Ctx _ctx;
        private readonly ReactiveProperty<float> _fullness;
        private readonly ReactiveProperty<bool> _visible;
        private HealthBarView _view;

        public HealthBarEntity(Ctx ctx)
        {
            _ctx = ctx;
            _fullness = AddForDisposal(new ReactiveProperty<float>());
            _visible = AddForDisposal(new ReactiveProperty<bool>());
            
            CreateHealthBarPM();
            InitializeView(ctx.View);
        }

        private void InitializeView(HealthBarView view)
        {
            var ctx = new HealthBarView.Ctx()
            {
                Fullness = _fullness,
                Visible = _visible
            };
            view.SetCtx(ctx);
        }

        private void CreateHealthBarPM()
        {
            var ctx = new HealthBarPM.Ctx()
            {
                CurrentHealth = _ctx.CurrentHealth,
                StartingHealth = _ctx.StartingHealth,
                Fullness = _fullness,
                Visible = _visible
            };
            AddForDisposal(new HealthBarPM(ctx));
        }
    }
}
