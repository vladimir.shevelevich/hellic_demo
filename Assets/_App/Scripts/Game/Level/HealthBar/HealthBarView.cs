using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.Game.Level.HealthBar
{
    public class HealthBarView : MonoBehaviour
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<float> Fullness;
            public IReadOnlyReactiveProperty<bool> Visible;
        }

        [SerializeField] private GameObject _visual;
        [SerializeField] private Image _fill;

        public void SetCtx(Ctx ctx)
        {
            ctx.Fullness.Subscribe(fullness => _fill.fillAmount = fullness).AddTo(this);
            ctx.Visible.Subscribe(visible => _visual.SetActive(visible));
        }
    }
}
