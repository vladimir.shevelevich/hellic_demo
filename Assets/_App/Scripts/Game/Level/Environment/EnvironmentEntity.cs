﻿using System.Threading.Tasks;
using Protopunk.Tools;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace App.Game.Level.Environment
{
    public class EnvironmentEntity : BaseDisposable
    {
        public struct Ctx
        {
            public GameObject EnvironmentRef;
        }

        private readonly Ctx _ctx;

        private GameObject _view;

        public EnvironmentEntity(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            _view = Object.Instantiate(_ctx.EnvironmentRef);
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            
            if (_view != null)
                Object.Destroy(_view);
        }
    }
}