﻿using System.Threading.Tasks;
using App.Game;
using App.GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.Input
{
    public class InputReaderEntity : BaseDisposable
    {
        public struct Ctx
        {
            public ReactiveProperty<Vector2> JoystickInput;

            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
        }

        private readonly Ctx _ctx;
        private InputReaderView _readerView;

        public InputReaderEntity(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var prefab =_ctx.ContentProvider.InputReaderPrefabRef;
            if (prefab == null)
                return;

            var ctx = new InputReaderView.Ctx
            {
                JoystickInput = _ctx.JoystickInput,
            };

            _readerView = AddForDestroy(Object.Instantiate(prefab, _ctx.UiRoot).GetComponent<InputReaderView>());
            _readerView.SetCtx(ctx);
        }
    }
}