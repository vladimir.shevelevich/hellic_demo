﻿using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.Input
{
    [RequireComponent(typeof(FloatingJoystick))]
    public class InputReaderView : MonoBehaviour
    {
        private FloatingJoystick _joystick;

        public struct Ctx
        {
            public ReactiveProperty<Vector2> JoystickInput;
        }

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _joystick = GetComponent<FloatingJoystick>();

            Observable.EveryUpdate().Subscribe(_ =>
                    EveryUpdate()).AddTo(this);
        }

        private void EveryUpdate()
        {
            _ctx.JoystickInput.Value = _joystick.Direction;
        }
    }
}