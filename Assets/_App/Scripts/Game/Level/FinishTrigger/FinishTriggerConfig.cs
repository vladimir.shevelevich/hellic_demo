﻿using System;
using UnityEngine;

namespace App.Game.Level.FinishTrigger
{
    [Serializable]
    public class FinishTriggerConfig
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Size;
    }
}