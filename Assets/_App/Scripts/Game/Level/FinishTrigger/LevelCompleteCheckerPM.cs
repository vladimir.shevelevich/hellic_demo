﻿using Protopunk.Tools;
using UniRx;

namespace App.Game.Level.FinishTrigger
{
    public class LevelCompleteCheckerPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<int> RescuedHostages;
            public int RequiredHostageRescueCount;
            public ReactiveProperty<bool> CompleteIsPossible;
        }

        private readonly Ctx _ctx;

        public LevelCompleteCheckerPM(Ctx ctx)
        {
            _ctx = ctx;
            AddForDisposal(_ctx.RescuedHostages.Subscribe(Check));
        }

        private void Check(int hostagesCount) => 
            _ctx.CompleteIsPossible.Value = hostagesCount >= _ctx.RequiredHostageRescueCount;
    }
}