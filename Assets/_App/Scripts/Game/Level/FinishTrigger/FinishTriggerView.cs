﻿using DG.Tweening;
using Protopunk.Tools;
using TMPro;
using UniRx;
using UnityEngine;

namespace App.Game.Level.FinishTrigger
{
    public class FinishTriggerView : MonoBehaviour
    {
        public struct Ctx
        {
            public ReactiveTrigger Triggered;
            public LayerMask PlayerLayer;
            public IReadOnlyReactiveProperty<int> RescuedHostages;
            public int RequiredHostageRescueCount;
            public IReadOnlyReactiveProperty<bool> CompleteIsPossible;
        }

        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        public Vector3 Size
        {
            get => _halfSize * 2;
            set => _halfSize = value / 2f;
        }

        public Quaternion Rotation
        {
            get => transform.rotation;
            set => transform.rotation = value;
        }

        [SerializeField] private Vector3 _halfSize;
        [SerializeField] private TMP_Text _hostageCountLabel;
        [SerializeField] private GameObject _hostagesUiVisual;
        [SerializeField] private SpriteRenderer _flashSprite;
        
        readonly Collider[] _hitsResult = new Collider[1];
        
        private Ctx _ctx;
        private Transform _transform;
        private bool _triggeredOnPreviousUpdate;
        
        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _transform = transform;

            _hostagesUiVisual.SetActive(_ctx.RequiredHostageRescueCount > 0);
            _ctx.RescuedHostages.Subscribe(count =>
            {
                _hostageCountLabel.text = $"{count}/{_ctx.RequiredHostageRescueCount}";
            }).AddTo(this);
            
            Observable.EveryUpdate().Subscribe(_ => EveryUpdate()).AddTo(this);
        }

        private void EveryUpdate()
        {
            if (!IsPlayerTriggered()) 
                return;

            if (_ctx.CompleteIsPossible.Value)
            {
                _ctx.Triggered?.Invoke();
                return;
            }

            AnimateFlash();
        }

        private void AnimateFlash()
        {
            var sequence = DOTween.Sequence();
            sequence.Append(_flashSprite.DOFade(1, 0.1f));
            sequence.Append(_flashSprite.DOFade(0, 0.1f));
            sequence.Append(_flashSprite.DOFade(1, 0.1f));
            sequence.Append(_flashSprite.DOFade(0, 0.1f));
            sequence.Append(_flashSprite.DOFade(1, 0.1f));
            sequence.Append(_flashSprite.DOFade(0, 0.1f));
        }

        private bool IsPlayerTriggered()
        {
            int hitCount = Physics.OverlapBoxNonAlloc(_transform.position, _halfSize,
                _hitsResult, Quaternion.identity, _ctx.PlayerLayer);

            var triggeredNow = hitCount != 0;

            switch (triggeredNow)
            {
                case false:
                    _triggeredOnPreviousUpdate = false;
                    return false;
                case true when _triggeredOnPreviousUpdate:
                    return false;
                default:
                    _triggeredOnPreviousUpdate = true;
                    return true;
            }
        }
    }
}