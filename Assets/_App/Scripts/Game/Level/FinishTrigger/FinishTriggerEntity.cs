﻿using System.Threading.Tasks;
using App.GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.FinishTrigger
{
    public class FinishTriggerEntity : BaseDisposable
    {
        public struct Ctx
        {
            public ReactiveTrigger LevelFinished;
            public ContentProvider ContentProvider;
            public Vector3 FinishPosition;
            public IReadOnlyReactiveProperty<int> RescuedHostages;
            public LevelConfig LevelConfig;
        }

        private readonly Ctx _ctx;
        private FinishTriggerView _view;
        private ReactiveProperty<bool> _completeIsPossible;

        public FinishTriggerEntity(Ctx ctx)
        {
            _ctx = ctx;

            _completeIsPossible = AddForDisposal(new ReactiveProperty<bool>());
            
            CreateLevelCompleteCheckerPM();
        }

        private void CreateLevelCompleteCheckerPM()
        {
            var ctx = new LevelCompleteCheckerPM.Ctx
            {
                CompleteIsPossible = _completeIsPossible,
                RescuedHostages = _ctx.RescuedHostages,
            };
            AddForDisposal(new LevelCompleteCheckerPM(ctx));
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var asset =_ctx.ContentProvider.FinishTriggerRef;
            
            if (asset == null)
                return;

            var ctx = new FinishTriggerView.Ctx
            {
                Triggered = _ctx.LevelFinished,
                PlayerLayer = _ctx.ContentProvider.Settings.PlayerSettings.PlayerLayer,
                CompleteIsPossible = _completeIsPossible,
                RescuedHostages = _ctx.RescuedHostages,
            };

            _view = AddForDestroy(Object.Instantiate(asset.GetComponent<FinishTriggerView>(), 
                _ctx.FinishPosition, Quaternion.identity));

            _view.SetCtx(ctx);
        }
    }
}