﻿using UnityEngine;

namespace App.Game.Level.FinishTrigger
{
    public class FinishTriggerConstructor : MonoBehaviour
    {
        [SerializeField] private FinishTriggerConfig _config;
        [SerializeField] private FinishTriggerView _view;
        
        public FinishTriggerConfig CollectConfig()
        {
            _config.Position = _view.Position;
            _config.Rotation = _view.Rotation;
            _config.Size = _view.Size;

            return _config;
        }
    }
}