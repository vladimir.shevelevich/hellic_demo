﻿namespace App.Game.Level
{
    public enum LevelState
    {
        InProcess = 0,
        Completed = 1,
        Paused = 2
    }
}