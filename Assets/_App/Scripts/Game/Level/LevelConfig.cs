﻿using App.Game.Level.FinishTrigger;
using App.Scripts.Game.Level.Enemy;
using App.Scripts.Game.Level.Player;
using UnityEngine;

namespace App.Game.Level
{
    [CreateAssetMenu(fileName = "NewLevel", menuName = "GameData/Level")]
    public class LevelConfig : ScriptableObject
    {
        public GameObject EnvironmentPrefabRef;
        public EnemyConfig[] Enemies;
        public FinishTriggerConfig FinishTriggerConfig;
        public PlayerConfig PlayerConfig;
    }
}