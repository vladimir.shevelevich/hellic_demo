﻿using System;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.TutorialScreen
{
    public class TutorialScreenView : MonoBehaviour
    {
        [SerializeField] private GameObject _tutorialVisual;
        
        public struct Ctx
        {
            public IReadOnlyReactiveTrigger OnLevelLoaded;
        }

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _ctx.OnLevelLoaded.Subscribe(OnLevelLoaded).AddTo(this);
        }

        private void OnLevelLoaded()
        {
            HideTutorialWithDelay(2f);
        }

        private void HideTutorialWithDelay(float delay)
        {
            Observable.Timer(TimeSpan.FromSeconds(delay)).Subscribe(_ =>
                _tutorialVisual.SetActive(false)).AddTo(this);
        }
    }
}