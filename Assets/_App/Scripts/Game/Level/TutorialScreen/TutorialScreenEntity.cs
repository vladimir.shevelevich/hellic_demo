﻿using System.Threading.Tasks;
using App.GameData;
using Protopunk.Tools;
using UnityEngine;

namespace App.Game.Level.TutorialScreen
{
    public class TutorialScreenEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveTrigger OnLevelLoaded;
            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
        }

        private readonly Ctx _ctx;

        public TutorialScreenEntity(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var asset = _ctx.ContentProvider.TutorialScreenRef;

            if (asset == null)
                return;

            var ctx = new TutorialScreenView.Ctx
            {
                OnLevelLoaded = _ctx.OnLevelLoaded
            };

            var view = Object.Instantiate(asset.GetComponent<TutorialScreenView>(), _ctx.UiRoot);
            AddForDestroy(view).SetCtx(ctx);
        }
    }
}