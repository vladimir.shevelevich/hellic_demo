using System.Threading.Tasks;
using App.GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.LevelCompleteScreen
{
    public class LevelCompleteScreenEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveTrigger PlayerFinished;
            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
            public ReactiveTrigger OnNextLevelButtonClicked;
            public IReadOnlyReactiveProperty<int> RewardedMoneyOnLevel;
        }

        private readonly Ctx _ctx;

        public LevelCompleteScreenEntity(Ctx ctx)
        {
            _ctx = ctx;
        }
        
        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var asset = _ctx.ContentProvider.LevelCompleteScreenRef;
            
            if (asset == null)
                return;

            var ctx = new LevelCompleteScreenView.Ctx
            {
                PlayerFinished = _ctx.PlayerFinished,
                RewardedMoneyOnLevel = _ctx.RewardedMoneyOnLevel,
                OnNextLevelButtonClicked = _ctx.OnNextLevelButtonClicked
            };

            AddForDestroy(Object.Instantiate(asset.GetComponent<LevelCompleteScreenView>(), _ctx.UiRoot))
                .SetCtx(ctx);
        }
    }
}
