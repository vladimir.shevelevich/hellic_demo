using Protopunk.Tools;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.Game.Level.LevelCompleteScreen
{
    public class LevelCompleteScreenView : MonoBehaviour
    {
        public struct Ctx
        {
            public IReadOnlyReactiveTrigger PlayerFinished;
            public IReadOnlyReactiveProperty<int> RewardedMoneyOnLevel;
            public ReactiveTrigger OnNextLevelButtonClicked;
        }

        [SerializeField] private Button _nextLevelButton;
        [SerializeField] private GameObject _moneyResultVisual;
        [SerializeField] private TMP_Text _resultMoneyText;

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;

            _ctx.PlayerFinished.Subscribe(Show).AddTo(this);

            Hide();
        }

        private void OnEnable() => 
            _nextLevelButton.onClick.AddListener(OnNextButtonClicked);

        private void OnDisable() => 
            _nextLevelButton.onClick.RemoveListener(OnNextButtonClicked);

        private void OnNextButtonClicked()
        {
            Hide();
            _ctx.OnNextLevelButtonClicked.Invoke();
        }

        private void Show()
        {
            gameObject.SetActive(true);

            _resultMoneyText.text = _ctx.RewardedMoneyOnLevel.Value.ToString();
            _moneyResultVisual.SetActive(_ctx.RewardedMoneyOnLevel.Value > 0);
        }

        private void Hide() => 
            gameObject.SetActive(false);
    }
}
