﻿using System;
using DG.Tweening;
using TMPro;
using UniRx;
using UnityEngine;

namespace App.Game.Level.LevelHud
{
    public abstract class AmountView<T> : MonoBehaviour where T : IComparable
    {
        private static readonly Vector3 VFXLabelScale = new Vector3(1.4f, 1.4f, 1f);
        private const float ScalingTime = 0.1f;

        public struct Ctx
        {
            public IReadOnlyReactiveProperty<T> Amount;
        }

        [SerializeField] private TMP_Text _label;
        private T _currentAmount;

        public void SetCtx(Ctx ctx)
        {
            ctx.Amount.Subscribe(UpdateAmount).AddTo(this);
        }
        
        private void UpdateAmount(T newAmount)
        {
            _label.text = newAmount.ToString();

            if (newAmount.CompareTo(_currentAmount) > 0)
            {
                PlayAmountUpdateVFX(_label.transform);
            }

            _currentAmount = newAmount;
        }
        
        private static void PlayAmountUpdateVFX(Transform labelTransform)
        {
            if (DOTween.IsTweening(labelTransform))
                return;

            labelTransform.DOScale(VFXLabelScale, ScalingTime)
                .OnComplete(() => labelTransform.DOScale(Vector3.one, ScalingTime));
        }
    }
}