﻿using GameData;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.Game.Level.LevelHud
{
    public class LevelHudView : MonoBehaviour
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<int> RescuedHostages;
            public int LevelIndex;
            public IReadOnlyReactiveProperty<float> RocketCooldown;
            public PlayerSettings PlayerSettings;
            public IReadOnlyReactiveProperty<int> MoneyAmount;
        }

        [SerializeField] private AmountView<int> _rescuedHostagesAmount;
        [SerializeField] private AmountView<int> _moneyAmount;
        [SerializeField] private TMP_Text _levelNumber;
        [SerializeField] private Image _rocketCooldownFill;

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;

            InitializeResqueHostagesAmount();
            InitializeMoneyAmount();

            _levelNumber.text = $"Level {_ctx.LevelIndex + 1}";
            
            _ctx.RocketCooldown.Subscribe(cooldown =>
            {
                _rocketCooldownFill.fillAmount = 1f - cooldown / _ctx.PlayerSettings.RocketCooldown;
            }).AddTo(this);
        }

        private void InitializeMoneyAmount()
        {
            var ctx = new IntAmountView.Ctx
            {
                Amount = _ctx.MoneyAmount
            };
            _moneyAmount.SetCtx(ctx);
        }

        private void InitializeResqueHostagesAmount()
        {
            var ctx = new IntAmountView.Ctx
            {
                Amount = _ctx.RescuedHostages
            };
            _rescuedHostagesAmount.SetCtx(ctx);
        }
    }
}