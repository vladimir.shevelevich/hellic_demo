﻿using System.Threading.Tasks;
using App.GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.LevelHud
{
    public class LevelHudEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<int> RescuedHostages;
            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
            public int LevelIndex;
            public IReadOnlyReactiveProperty<float> RocketCooldown;
            public IReadOnlyReactiveProperty<int> MoneyAmount;
        }

        private readonly Ctx _ctx;

        public LevelHudEntity(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var asset = _ctx.ContentProvider.LevelHudRef;
            
            if (asset == null)
                return;

            var ctx = new LevelHudView.Ctx
            {
                RescuedHostages = _ctx.RescuedHostages,
                LevelIndex = _ctx.LevelIndex,
                MoneyAmount = _ctx.MoneyAmount,
                RocketCooldown = _ctx.RocketCooldown,
                PlayerSettings = _ctx.ContentProvider.Settings.PlayerSettings
            };

            var view = Object.Instantiate(asset.GetComponent<LevelHudView>(), _ctx.UiRoot);
            AddForDestroy(view).SetCtx(ctx);
        }
    }
}