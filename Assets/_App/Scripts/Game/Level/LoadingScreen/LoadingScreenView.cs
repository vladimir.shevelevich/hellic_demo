using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.LoadingScreen
{
    public class LoadingScreenView : MonoBehaviour
    {
        public struct Ctx
        {
            public IReadOnlyReactiveTrigger LevelLoaded;
            public IReadOnlyReactiveTrigger OnRestartLevelButtonClicked;
            public IReadOnlyReactiveTrigger OnNextLevelButtonClicked;
        }

        private Ctx _ctx;

        [SerializeField] private Animator _loadingSpinAnimator;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _ctx.LevelLoaded.Subscribe(Hide).AddTo(this);
            _ctx.OnNextLevelButtonClicked.Subscribe(ShowBeforeLoading).AddTo(this);
            _ctx.OnRestartLevelButtonClicked.Subscribe(ShowBeforeLoading).AddTo(this);
        }

        private void ShowBeforeLoading()
        {
            _loadingSpinAnimator.enabled = false;
            gameObject.SetActive(true);
        }

        private void Hide() => 
            gameObject.SetActive(false);
    }
}