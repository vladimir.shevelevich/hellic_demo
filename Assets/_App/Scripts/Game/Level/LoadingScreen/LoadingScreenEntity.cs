﻿using System.Threading.Tasks;
using App.GameData;
using Protopunk.Tools;
using UnityEngine;

namespace App.Game.Level.LoadingScreen
{
    public class LoadingScreenEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveTrigger LevelLoaded;
            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
            public IReadOnlyReactiveTrigger OnRestartLevelButtonClicked;
            public IReadOnlyReactiveTrigger OnNextLevelButtonClicked;
        }

        private readonly Ctx _ctx;

        public LoadingScreenEntity(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var asset = _ctx.ContentProvider.LoadingScreenRef;
            
            if (asset == null)
                return;

            var ctx = new LoadingScreenView.Ctx
            {
                LevelLoaded = _ctx.LevelLoaded,
                OnRestartLevelButtonClicked = _ctx.OnRestartLevelButtonClicked,
                OnNextLevelButtonClicked = _ctx.OnNextLevelButtonClicked
            };

            var view = Object.Instantiate(asset.GetComponent<LoadingScreenView>(), _ctx.UiRoot);
            AddForDestroy(view).SetCtx(ctx);
        }
    }
}