#if UNITY_EDITOR

using System.Linq;
using App.Game.Level.FinishTrigger;
using App.Scripts.Game.Level.Enemy;
using App.Scripts.Game.Level.Player;
using UnityEditor;
using UnityEngine;

namespace App.Game.Level
{
    [CustomEditor(typeof(LevelConfig))]
    public class LevelConfigEditor : Editor
    {
        private LevelConfig _levelConfig;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            _levelConfig = target as LevelConfig;

            DrawSaveButtons();
        }

        private void DrawSaveButtons()
        {
            GUILayout.Space(EditorGUIUtility.standardVerticalSpacing);

            if (GUILayout.Button("SAVE LEVEL FROM SCENE"))
            {
                SaveLevelConfig();
                Debug.Log("Level data has been saved successfully");
            }
        }

        private void SaveLevelConfig()
        {
            SaveFinishTriggerConfig();
            SaveEnemiesConfig();
            SavePlayerConfig();

            EditorUtility.SetDirty(target);
        }

        private void SavePlayerConfig()
        {
            var constructor = FindObjectOfType<PlayerConstructor>();
            _levelConfig.PlayerConfig = constructor.CollectConfig();
        }

        private void SaveEnemiesConfig()
        {
            var constructors = FindObjectsOfType<EnemyConstructor>();
            _levelConfig.Enemies = constructors.Select(x => x.CollectConfig()).ToArray();
        }

        private void SaveFinishTriggerConfig()
        {
            var constructor = FindObjectOfType<FinishTriggerConstructor>();
            _levelConfig.FinishTriggerConfig = constructor.CollectConfig();
        }
    }
}
#endif