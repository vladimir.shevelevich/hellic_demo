using App.Game.Level.FinishTrigger;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace App.Scripts.Game.Level.Editor
{
    [CustomEditor(typeof(FinishTriggerView))]
    public class FinishTriggerEditor : UnityEditor.Editor
    {
        private readonly BoxBoundsHandle _boundsHandle = new BoxBoundsHandle();

        private FinishTriggerView _view;
        
        private void OnEnable()
        {
            _view = (FinishTriggerView)target;
            _boundsHandle.SetColor(Color.magenta);
        }

        protected void OnSceneGUI()
        {
            var position = _view.Position;
            var size = _view.Size;
            
            DrawRotatedBoxBoundsHandle(ref position, ref size, _view.Rotation);

            _view.Position = position;
            _view.Size = size;
        }

        private void DrawRotatedBoxBoundsHandle(ref Vector3 position, ref Vector3 size, Quaternion rotation)
        {
            var matrix = Matrix4x4.TRS(Vector3.zero, rotation, Vector3.one);
            
            using (new Handles.DrawingScope(matrix))
            {
                _boundsHandle.center = matrix.inverse.MultiplyPoint3x4(position);
                _boundsHandle.size = size;
 
                EditorGUI.BeginChangeCheck();
                
                _boundsHandle.DrawHandle();
                
                if (EditorGUI.EndChangeCheck())
                {
                    position = matrix.MultiplyPoint3x4(_boundsHandle.center);
                    size = _boundsHandle.size;
                }
            }
        }
    }
}
