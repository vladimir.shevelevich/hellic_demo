﻿using App.Game.Level.Common;
using App.GameData;
using Protopunk.Tools;
using UnityEngine;

namespace App.Game.Level.Rocket
{
    public class RocketEntity : BaseDisposable
    {
        public struct Ctx
        {
            public GameObject RocketPrefab;
            public Vector3 TargetPosition;

            public Vector3 RocketStartPosition;
            public float RocketDamage;
            public float RocketSpeed;
            public ContentProvider ContentProvider;
        }

        private Ctx _ctx;
        private RocketView _view;

        private readonly ReactiveTrigger _onRocketHit;
        private readonly ReactiveEvent<IDamageable> _onDamageableHit;
        private readonly ReactiveTrigger _playDestroy;

        public RocketEntity(Ctx ctx)
        {
            _ctx = ctx;
            _onRocketHit = AddForDisposal(new ReactiveTrigger());
            _onDamageableHit = AddForDisposal(new ReactiveEvent<IDamageable>());
            _playDestroy = AddForDisposal(new ReactiveTrigger());

            CreatePM();
            CreateView();
        }

        private void CreatePM()
        {
            var ctx = new RocketPM.Ctx
            {
                OnDamageableHit = _onDamageableHit,
                PlayDestroy = _playDestroy,
                RocketDamage = _ctx.RocketDamage
            };
            AddForDisposal(new RocketPM(ctx));
        }

        private void CreateView()
        {
            var ctx = new RocketView.Ctx
            {
                OnRocketHit = _onRocketHit,
                OnDamageableHit = _onDamageableHit,
                PlayDestroy = _playDestroy,
                TargetPosition = _ctx.TargetPosition,
                RocketSpeed = _ctx.RocketSpeed,
                ContentProvider = _ctx.ContentProvider
            };

            _view = AddForDestroy(Object.Instantiate(_ctx.RocketPrefab, _ctx.RocketStartPosition, Quaternion.identity)
                .GetComponent<RocketView>());
            _view.SetCtx(ctx);
        }
    }
}