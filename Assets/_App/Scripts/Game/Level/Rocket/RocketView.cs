﻿using App.Game.Level.Common;
using App.GameData;
using MoreMountains.NiceVibrations;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.Rocket
{
    public class RocketView : MonoBehaviour
    {
        [SerializeField] private GameObject _visual;

        public struct Ctx
        {
            public ReactiveTrigger OnRocketHit;
            public ReactiveEvent<IDamageable> OnDamageableHit;
            public IReadOnlyReactiveTrigger PlayDestroy;

            public Vector3 TargetPosition;
            public float RocketSpeed;
            public ContentProvider ContentProvider;
        }

        private Ctx _ctx;
        private bool _destroyed;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _ctx.PlayDestroy.Subscribe(PlayRocketDestroy).AddTo(this);
            Observable.EveryUpdate().Subscribe(_ => EveryUpdate()).AddTo(this);
            LookAtTarget();
        }

        private void LookAtTarget()
        {
            transform.LookAt(_ctx.TargetPosition);
        }

        private void EveryUpdate()
        {
            if (!_destroyed)
                MoveToTarget();
        }

        private void MoveToTarget()
        {
            transform.position += transform.forward * _ctx.RocketSpeed * Time.deltaTime;
        }

        private void OnTriggerEnter(Collider other)
        {
            _ctx.OnRocketHit.Invoke();
            if (other.TryGetComponent(out IDamageable damageable))
                _ctx.OnDamageableHit.Invoke(damageable);
        }

        private void PlayRocketDestroy()
        {
            _visual.SetActive(false);
            GetComponent<Collider>().enabled = false;
            Instantiate(_ctx.ContentProvider.Vfx.RocketExplodeVfx, transform.position, Quaternion.identity, transform);
            _destroyed = true;
            MMVibrationManager.Haptic(HapticTypes.MediumImpact);
        }
    }
}