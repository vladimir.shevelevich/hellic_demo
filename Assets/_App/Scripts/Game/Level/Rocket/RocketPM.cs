﻿using App.Game.Level.Common;
using Protopunk.Tools;

namespace App.Game.Level.Rocket
{
    public class RocketPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveEvent<IDamageable> OnDamageableHit;
            public ReactiveTrigger PlayDestroy;

            public float RocketDamage;
        }

        private readonly Ctx _ctx;

        public RocketPM(Ctx ctx)
        {
            _ctx = ctx;

            AddForDisposal(_ctx.OnDamageableHit.SubscribeWithSkip(OnDamageableHit));
        }

        private void OnDamageableHit(IDamageable damageable)
        {
            if (!damageable.IsAlive)
                return;

            _ctx.PlayDestroy.Invoke();
            damageable.ApplyDamage(_ctx.RocketDamage);
        }
    }
}