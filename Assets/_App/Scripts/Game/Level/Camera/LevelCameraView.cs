﻿using Cinemachine;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.Camera
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class LevelCameraView : MonoBehaviour
    {
        private CinemachineVirtualCamera _virtualCamera;

        public struct Ctx
        {
            public ReactiveProperty<Transform> target;
        }

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _virtualCamera = GetComponent<CinemachineVirtualCamera>();

            _ctx.target.Subscribe(SetTarget).AddTo(this);
        }

        private void SetTarget(Transform target)
        {
            _virtualCamera.Follow = target;
            _virtualCamera.LookAt = target;
        }
    }
}