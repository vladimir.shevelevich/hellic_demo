﻿using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.Camera
{
    public class LevelCameraPM : BaseDisposable
    {
        public struct Ctx
        {
            public ReactiveProperty<Transform> CameraTarget;
            public IReadOnlyReactiveProperty<Transform> PlayerCameraTarget;
        }

        private readonly Ctx _ctx;

        public LevelCameraPM(Ctx ctx)
        {
            _ctx = ctx;
            AddForDisposal(_ctx.PlayerCameraTarget.Subscribe(
                SetTarget));
        }

        private void SetTarget(Transform playerTransform)
        {
            _ctx.CameraTarget.Value = playerTransform;
        }
    }
}