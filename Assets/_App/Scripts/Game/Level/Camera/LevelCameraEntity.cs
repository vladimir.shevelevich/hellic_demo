﻿using System.Threading.Tasks;
using App.Game;
using App.GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.Camera
{
    public class LevelCameraEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<Transform> PlayerCameraTarget;
            public ContentProvider ContentProvider;
        }

        private readonly Ctx _ctx;
        private LevelCameraView _view;

        private readonly ReactiveProperty<Transform> _cameraTarget = new ReactiveProperty<Transform>();

        public LevelCameraEntity(Ctx ctx)
        {
            _ctx = ctx;

            CreatePM();
        }

        private void CreatePM()
        {
            var ctx = new LevelCameraPM.Ctx
            {
                PlayerCameraTarget = _ctx.PlayerCameraTarget,
                CameraTarget = _cameraTarget
            };
            AddForDisposal(new LevelCameraPM(ctx));
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
        }

        private async Task CreateViewAsync()
        {
            var prefab = _ctx.ContentProvider.LevelCameraPrefabRef;
            if (prefab == null)
                return;

            _view = AddForDestroy(Object.Instantiate(prefab.GetComponent<LevelCameraView>()));
            var ctx = new LevelCameraView.Ctx
            {
                target = _cameraTarget
            };
            _view.SetCtx(ctx);
        }
    }
}