﻿using Protopunk.Tools;
using UnityEngine;

namespace App.Scripts.Game.Level.DamageNumbers
{
    public class DamageNumberEntity : BaseDisposable
    {
        public struct Ctx
        {
            public GameObject Prefab;
            public int Damage;
            public Vector3 SpawnPoint;
        }

        private readonly Ctx _ctx;

        public DamageNumberEntity(Ctx ctx)
        {
            _ctx = ctx;
            CreateView();
        }

        private void CreateView()
        {
            var ctx = new DamageNumberView.Ctx
            {
                Damage = _ctx.Damage
            };

            var view = Object.Instantiate(_ctx.Prefab.GetComponent<DamageNumberView>(), _ctx.SpawnPoint, Quaternion.identity);
            AddForDestroy(view).SetCtx(ctx);
        }
    }
}