﻿using System;
using System.Collections.Generic;
using Protopunk.Tools;
using UniRx;

namespace App.Game.Level
{
    public class LevelPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<bool> IsPlayerAlive;
            public IReadOnlyReactiveTrigger OnPlayerFinished;
            public int LevelIndex;
            public IReadOnlyReactiveTrigger OnHostageRescued;
            public ReactiveProperty<int> SavedHostages;
            public ReactiveProperty<LevelState> LevelState;
            public ReactiveTrigger LevelCompleted;
            public ReactiveTrigger RestartLevel;
            public IReadOnlyReactiveTrigger OnNextLevelButtonClicked;
            public IReadOnlyReactiveTrigger OnRestartLevelButtonClicked;
            public IReadOnlyReactiveEvent<int> OnMoneyRewarded;
            public ReactiveProperty<int> MoneyAmount;
            public ReactiveProperty<int> RewardedMoneyOnLevel;
        }

        private readonly Ctx _ctx;
        private readonly Dictionary<string, object> _analyticsData = new();

        public LevelPM(Ctx ctx)
        {
            _ctx = ctx;
            
            SetLevelState(LevelState.InProcess);

            SendAnalyticsEvent("level_start");
            AddForDisposal(_ctx.IsPlayerAlive.Skip(1).Subscribe(isAlive =>
            {
                if (isAlive)
                    return;
                
                SetLevelState(LevelState.Completed);
                SendAnalyticsEvent("level_fail");
            }));
            AddForDisposal(_ctx.OnPlayerFinished.Subscribe(() =>
            {
                SetLevelState(LevelState.Completed);
                SendAnalyticsEvent("level_complete");
            }));
            AddForDisposal(_ctx.OnHostageRescued.Subscribe(OnHostagesSaved));

            AddForDisposal(_ctx.OnNextLevelButtonClicked.Subscribe(OnNextLevelButtonClicked));
            AddForDisposal(_ctx.OnRestartLevelButtonClicked.Subscribe(OnRestartLevelButtonClicked));
            AddForDisposal(_ctx.OnMoneyRewarded.Subscribe(OnMoneyRewarded));
        }

        private void OnMoneyRewarded(int amount)
        {
            if (amount <= 0)
                return;

            _ctx.MoneyAmount.Value += amount;
            _ctx.RewardedMoneyOnLevel.Value += amount;
        }
        
        private void OnRestartLevelButtonClicked() =>
            AddForDisposal(Observable
                .Timer(TimeSpan.FromSeconds(0.3f))
                .Subscribe(_ => _ctx.RestartLevel.Invoke()));

        private void OnNextLevelButtonClicked() => 
            AddForDisposal(Observable
                .Timer(TimeSpan.FromSeconds(0.3f))
                .Subscribe(_ => _ctx.LevelCompleted.Invoke()));

        private void OnHostagesSaved() => 
            ++_ctx.SavedHostages.Value;
        
        private void SendAnalyticsEvent(string eventName)
        {
            _analyticsData.Clear();
            _analyticsData.Add("level_number", _ctx.LevelIndex+1);
            AppMetrica.Instance.ReportEvent(eventName, _analyticsData);
            AppMetrica.Instance.SendEventsBuffer();
        }
        
        private void SetLevelState(LevelState levelState) => 
            _ctx.LevelState.Value = levelState;
    }
}