﻿using Protopunk.Tools;
using UnityEngine;

namespace App.Game.Level.PopupNotification
{
    public class PopupNotificationEntity : BaseDisposable
    {
        public struct Ctx
        {
            public GameObject Prefab;
            public string Text;
            public Vector3 SpawnPoint;
        }

        private readonly Ctx _ctx;

        public PopupNotificationEntity(Ctx ctx)
        {
            _ctx = ctx;
            CreateView();
        }
        
        private void CreateView()
        {
            var ctx = new PopupNotificationView.Ctx
            {
                Text = _ctx.Text
            };

            var view = Object.Instantiate(_ctx.Prefab.GetComponent<PopupNotificationView>(), _ctx.SpawnPoint, Quaternion.identity);
            AddForDestroy(view).SetCtx(ctx);
        }
    }
}