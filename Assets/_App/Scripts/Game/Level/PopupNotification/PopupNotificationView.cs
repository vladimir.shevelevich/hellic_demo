using DG.Tweening;
using TMPro;
using UnityEngine;

namespace App.Game.Level.PopupNotification
{
    public class PopupNotificationView : MonoBehaviour
    {
        public struct Ctx
        {
            public string Text;
        }

        private Transform _transform;
        
        [SerializeField] private TMP_Text _label;
        [SerializeField] private float _startOffset = 1f;
        [SerializeField] private float _liftingDistance = 2f;
        [SerializeField] private float _animationTime = 0.5f;

        public void SetCtx(Ctx ctx)
        {
            _transform = transform;
            _label.text = ctx.Text;
            Animate();
        }

        private void Animate()
        {
            var sequence = DOTween.Sequence();
            var position = _transform.position;

            LookAtCamera();

            var upDirection = _transform.up;
            position += upDirection * _startOffset;
            _transform.position = position;

            sequence.Append(_transform.DOMove(position + upDirection * _liftingDistance, _animationTime));
            sequence.Join(_label.DOFade(0f, _animationTime));
            sequence.OnComplete(() => gameObject.SetActive(false));
        }

        private void LookAtCamera()
        {
            var rotation = UnityEngine.Camera.main.transform.rotation;
            _transform.LookAt(_transform.position + rotation * Vector3.forward, rotation * Vector3.up);
        }
    }
}