﻿using System;
using System.Collections.Generic;
using App.Game.Level;
using App.Game.Level.Common;
using App.GameData.Settings;
using GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace _App.Scripts.Game.Level.Player
{
    public class PlayerAttackerPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveEvent<List<IDamageable>> OnDamageableTrigger;
            public IReadOnlyReactiveProperty<Vector2> MovementInput;
            public IReadOnlyReactiveProperty<float> PlayerVelocity;
            public IReadOnlyReactiveProperty<Transform> PlayerTransform;

            public ReactiveEvent<List<IDamageable>> OnRocketTargetingStart;
            public ReactiveTrigger OnRocketTargetingStop;
            public ReactiveEvent<List<IDamageable>> UseRockets;
            public ReactiveEvent<IDamageable> OnGunUsed;
            public ReactiveProperty<IDamageable> CapturedTarget;

            public PlayerSettings PlayerSettings;
            public IReadOnlyReactiveProperty<LevelState> LevelState;
            public ReactiveProperty<float> RocketCooldown;
            public IReadOnlyReactiveProperty<int> DamageLevelUpgrade;
        }

        private readonly Ctx _ctx;

        private float _lastGunUseTime = float.MinValue;
        private float _lastRocketUseTime = float.MinValue;
        private IDisposable _rocketTargetingProcess;
        private bool _damageableTriggeredInThisUpdate;

        public PlayerAttackerPM(Ctx ctx)
        {
            _ctx = ctx;
            AddForDisposal(_ctx.OnDamageableTrigger.SubscribeWithSkip(OnDamageablesTrigger));
            AddForDisposal(_ctx.MovementInput.Subscribe(OnPlayerMoveInputChange));
            AddForDisposal(Observable.EveryLateUpdate().Subscribe(_=> EveryLateUpdate()));
            AddForDisposal(Observable.EveryUpdate().Subscribe(_ => EveryUpdate()));
        }

        private void EveryUpdate()
        {
            UpdateRocketCooldown();
        }

        private void EveryLateUpdate()
        {
            CheckForTargetCapture();
            _damageableTriggeredInThisUpdate = false;
        }

        private void UpdateRocketCooldown()
        {
            if (_ctx.RocketCooldown.Value == 0)
                return;

            var elapsedTime = Time.realtimeSinceStartup - _lastRocketUseTime;
            _ctx.RocketCooldown.Value = Mathf.Clamp(
                _ctx.PlayerSettings.RocketCooldown - elapsedTime,
                0f,
                _ctx.PlayerSettings.RocketCooldown);
        }

        private void CheckForTargetCapture()
        {
            if (!_damageableTriggeredInThisUpdate || _ctx.PlayerVelocity.Value == 0)
                _ctx.CapturedTarget.Value = null;
        }

        private void OnDamageablesTrigger(List<IDamageable> damageables)
        {
            if (!CanAttack())
                return;
            
            _damageableTriggeredInThisUpdate = true;
            if (IsGunAvailable())
                UseGun(damageables);
            if (_ctx.MovementInput.Value == Vector2.zero && IsRocketAvailable())
                StartRocketTargeting(damageables);
        }

        private void OnPlayerMoveInputChange(Vector2 MoveInput)
        {
            if (_ctx.MovementInput.Value != Vector2.zero && _rocketTargetingProcess != null)
                StopRocketTargeting();
        }

        private void UseGun(List<IDamageable> damageables)
        {
            FilterLowPriorityDamageables(damageables);
            var damageableTarget = GetClosestDamageable(damageables);
            var damage = _ctx.PlayerSettings.GunDamage;
            damageableTarget.ApplyDamage(damage);
            _ctx.CapturedTarget.Value = damageableTarget;
            _ctx.OnGunUsed.Invoke(damageableTarget);
            _lastGunUseTime = Time.realtimeSinceStartup;
        }

        private void StartRocketTargeting(List<IDamageable> damageables)
        {
            _rocketTargetingProcess = RocketTargetingProcess(damageables);
            _ctx.OnRocketTargetingStart.Invoke(damageables);
        }

        private void StopRocketTargeting()
        {
            Debug.Log("Stop targeting");
            _rocketTargetingProcess.Dispose();
            _rocketTargetingProcess = null;
            _ctx.OnRocketTargetingStop.Invoke();
        }

        private IDisposable RocketTargetingProcess(List<IDamageable> damageables)
        {
            return AddForDisposal(Observable.Timer(TimeSpan.FromSeconds(_ctx.PlayerSettings.RocketTargetingDuration)).Subscribe(_ =>
                    OnRocketTargetingComplete(damageables)));
        }

        private void OnRocketTargetingComplete(List<IDamageable> damageables)
        {
            UseRockets(damageables);
        }

        private void UseRockets(List<IDamageable> damageables)
        {
            StopRocketTargeting();
            _ctx.UseRockets.Invoke(damageables);
            SetRocketCooldown();
        }

        private void SetRocketCooldown()
        {
            _ctx.RocketCooldown.Value = _ctx.PlayerSettings.RocketCooldown;
            _lastRocketUseTime = Time.realtimeSinceStartup;
        }

        private void FilterLowPriorityDamageables(List<IDamageable> damageables)
        {
            int topPriorityValue = damageables[0].AttackPriority;
            for (var i = 1; i < damageables.Count; i++)
            {
                if (damageables[i].AttackPriority < topPriorityValue)
                    topPriorityValue = damageables[i].AttackPriority;
            }

            for (int i = damageables.Count-1; i >= 0; i--)
            {
                var damageable = damageables[i];
                if (damageable.AttackPriority != topPriorityValue)
                    damageables.Remove(damageable);
            }
        }

        private IDamageable GetClosestDamageable(List<IDamageable> damageables)
        {
            IDamageable closestDamageable = null;
            float minDistance = float.MaxValue;
            foreach (var damageable in damageables)
            {
                var distanceToDamageable = Vector3.Distance(_ctx.PlayerTransform.Value.position, damageable.TargetTransform.position);
                if (distanceToDamageable < minDistance)
                {
                    closestDamageable = damageable;
                    minDistance = distanceToDamageable;
                }
            }
            return closestDamageable;
        }

        private bool CanAttack() => 
            _ctx.LevelState.Value == LevelState.InProcess;


        private bool IsGunAvailable()
        {
            return Time.realtimeSinceStartup - _lastGunUseTime > _ctx.PlayerSettings.GunCooldown;
        }

        private bool IsRocketAvailable()
        {
            return _ctx.RocketCooldown.Value == 0 &&
                   _rocketTargetingProcess == null;
        }
    }
}