﻿using System;
using System.Collections.Generic;
using _App.Scripts.Game.Level.Player;
using App.Game.Enums;
using App.Game.Level.Common;
using App.GameData;
using App.Scripts.Game.Level.HealthBar;
using DG.Tweening;
using GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.Player
{
    [RequireComponent(typeof(PlayerMoverView))]
    [RequireComponent(typeof(PlayerAttackerView))]
    public class PlayerView : MonoBehaviour, IDamageable
    {
        public HealthBarView HealthBarView => _healthBarView;

        [SerializeField] private GameObject _visual;
        [SerializeField] private HealthBarView _healthBarView;
        [SerializeField] private Transform _damageableTarget;
        [SerializeField] private Renderer _modelRenderer;
        [SerializeField] private Animator _animator;

        public struct Ctx
        {
            public IReadOnlyReactiveProperty<Vector2> MovementInput;
            public IReadOnlyReactiveEvent<IDamageable> OnGunUsed;
            public IReadOnlyReactiveEvent<List<IDamageable>> OnRocketTargetingStart;
            public IReadOnlyReactiveTrigger OnRocketTargetingStop;
            public IReadOnlyReactiveEvent<List<IDamageable>> UseRockets;
            public IReadOnlyReactiveProperty<bool> IsAlive;
            public IReadOnlyReactiveEvent<float> OnDamageReceived;
            public IReadOnlyReactiveProperty<IDamageable> CapturedTarget;

            public ReactiveEvent<float> ApplyDamage;
            public ReactiveEvent<List<IDamageable>> OnDamageableTrigger;
            public ReactiveProperty<float> PlayerVelocity;
            public ReactiveProperty<Transform> PlayerCameraTarget;
            public IReadOnlyReactiveProperty<int> DamageLevelUpgrade;

            public PlayerSettings PlayerSettings;
            public RectTransform UiRoot;
            public ContentProvider ContentProvider;
        }

        private Ctx _ctx;
        private static readonly int ModelEmissionProperty = Shader.PropertyToID("_EmissionColor");

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            InitMoverView();
            InitAttackerView();

            _ctx.IsAlive.Subscribe(isAlive =>
            {
                if (!isAlive)
                    PlayDeathWithDelay(.5f);
            }).AddTo(this);
            _ctx.OnDamageReceived.Subscribe(PlayDamage).AddTo(this);
        }

        public bool IsAlive => _ctx.IsAlive.Value;
        int IDamageable.AttackPriority => 0;
        Transform IDamageable.TargetTransform => _damageableTarget;

        private void InitMoverView()
        {
            var ctx = new PlayerMoverView.Ctx
            {
                MovementInput = _ctx.MovementInput,
                IsAlive = _ctx.IsAlive,
                PlayerCameraTarget = _ctx.PlayerCameraTarget,
                PlayerCurrentSpeed = _ctx.PlayerVelocity,
                CapturedTarget = _ctx.CapturedTarget,
                PlayerSettings = _ctx.PlayerSettings
            };
            GetComponent<PlayerMoverView>().SetCtx(ctx);
        }

        private void InitAttackerView()
        {
            var ctx = new PlayerAttackerView.Ctx
            {
                OnGunUsed = _ctx.OnGunUsed,
                OnDamageableTrigger = _ctx.OnDamageableTrigger,
                OnRocketTargetingStart = _ctx.OnRocketTargetingStart,
                OnRocketTargetingStop = _ctx.OnRocketTargetingStop,
                UseRockets = _ctx.UseRockets,
                DamageLevelUpgrade = _ctx.DamageLevelUpgrade,

                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot
            };
            GetComponent<PlayerAttackerView>().SetCtx(ctx);
        }

        void IDamageable.ApplyDamage(float damage)
        {
            _ctx.ApplyDamage.Invoke(damage);
        }

        private void PlayDamage(float damage)
        {
            _modelRenderer.material.DOKill();
            _modelRenderer.material.SetColor(ModelEmissionProperty, Color.black);
            _modelRenderer.material.DOColor(Color.white, ModelEmissionProperty, 0.1f).SetLoops(2, LoopType.Yoyo);
        }

        private void PlayDeathWithDelay(float delay)
        {
            Observable.Timer(TimeSpan.FromSeconds(delay)).Subscribe(_ =>
            {
                _animator.SetTrigger("Death");
                GetComponent<Collider>().enabled = false;
            }).AddTo(this);
        }
    }
}