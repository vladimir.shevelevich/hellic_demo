using System;
using UnityEngine;

namespace App.Scripts.Game.Level.Player
{
    [Serializable]
    public class PlayerConfig
    {
        public Vector3 Position;
        public Quaternion Rotation;
    }
}
