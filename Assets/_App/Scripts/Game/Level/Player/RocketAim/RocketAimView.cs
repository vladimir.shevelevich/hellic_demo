﻿using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace _App.Scripts.Game.Level.Player.RocketAim
{
    public class RocketAimView : MonoBehaviour
    {
        [SerializeField] private Image _image;

        public struct Ctx
        {
            public Transform TargetTransform;
            public float TargetingDuration;
        }

        private Ctx _ctx;
        private UnityEngine.Camera _camera;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _camera = UnityEngine.Camera.main;
            Observable.EveryUpdate().Subscribe(_ => EveryUpdate()).AddTo(this);
            PlaceOnTarget();
            PlayTargetingTween();
        }

        private void EveryUpdate()
        {
            PlaceOnTarget();
        }

        private void PlayTargetingTween()
        {
            var rect = GetComponent<RectTransform>();
            var targetRectSize = rect.rect.size * 0.5f;
            rect.DOSizeDelta(targetRectSize, _ctx.TargetingDuration).SetEase(Ease.Linear);
        }

        private void PlaceOnTarget()
        {
            var targetActive = _ctx.TargetTransform != null;
            _image.enabled = targetActive;
            if (targetActive)
                transform.position = _camera.WorldToScreenPoint(_ctx.TargetTransform.position);
        }
    }
}