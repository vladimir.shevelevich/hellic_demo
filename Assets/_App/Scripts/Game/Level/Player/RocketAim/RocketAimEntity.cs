﻿using Protopunk.Tools;
using UnityEngine;

namespace _App.Scripts.Game.Level.Player.RocketAim
{
    public class RocketAimEntity : BaseDisposable
    {
        public struct Ctx
        {
            public Transform TargetTransform;
            public RectTransform UiRoot;
            public GameObject RocketAimPrefab;
            public float TargetingDuration;
        }

        private readonly Ctx _ctx;
        private RocketAimView _view;

        public RocketAimEntity(Ctx ctx)
        {
            _ctx = ctx;
            CreateView();
        }

        private void CreateView()
        {
            var ctx = new RocketAimView.Ctx
            {
                TargetTransform = _ctx.TargetTransform,
                TargetingDuration = _ctx.TargetingDuration
            };
            _view = AddForDestroy(Object.Instantiate(_ctx.RocketAimPrefab, _ctx.UiRoot).GetComponent<RocketAimView>());
            _view.SetCtx(ctx);
        }
    }
}