﻿using System;
using App.Game.Level;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace Game.Level.Player
{
    public class PlayerMoverPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<Vector2> JoystickInput;
            public ReactiveProperty<Vector2> MovementInput;
            public IReadOnlyReactiveProperty<LevelState> LevelState;
        }

        private readonly Ctx _ctx;

        public PlayerMoverPM(Ctx ctx)
        {
            _ctx = ctx;
            AddForDisposal(_ctx.LevelState.Subscribe(_ => ReadJoystickInput(_ctx.JoystickInput.Value)));
            AddForDisposal(_ctx.JoystickInput.Subscribe(ReadJoystickInput));
        }

        private void ReadJoystickInput(Vector2 input)
        {
            switch (_ctx.LevelState.Value)
            {
                case LevelState.InProcess:
                    _ctx.MovementInput.Value = input;
                    break;
                case LevelState.Completed:
                case LevelState.Paused:
                    _ctx.MovementInput.Value = Vector2.zero;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}