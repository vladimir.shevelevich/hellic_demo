﻿using System.Collections.Generic;
using System.Threading.Tasks;
using _App.Scripts.Game.Level.Player;
using App.Game.Level.Common;
using App.GameData;
using App.Scripts.Game.Level.HealthBar;
using App.Scripts.Game.Level.Player;
using Game.Level.Player;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.Player
{
    public class PlayerEntity : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<Vector2> JoystickInput;
            public ReactiveProperty<Transform> PlayerCameraTarget;
            public ReactiveProperty<Transform> PlayerTransform;
            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
            public ReactiveProperty<bool> IsAlive;
            public PlayerConfig PlayerConfig;
            public IReadOnlyReactiveProperty<LevelState> LevelState;
            public ReactiveProperty<float> RocketCooldown;
        }

        private readonly Ctx _ctx;
        private PlayerView _view;

        private readonly ReactiveProperty<Vector2> _movementInput;
        private readonly ReactiveEvent<List<IDamageable>> _onDamageableTrigger;
        private readonly ReactiveProperty<float> _playerVelocity;
        private readonly ReactiveEvent<IDamageable> _onGunUsed;
        private readonly ReactiveEvent<List<IDamageable>> _onRocketTargetingStart;
        private readonly ReactiveTrigger _onRocketTargetingStop;
        private readonly ReactiveEvent<List<IDamageable>> _useRockets;
        private readonly ReactiveEvent<float> _applyDamage;
        private readonly ReactiveProperty<float> _currentHealth;
        private readonly ReactiveProperty<IDamageable> _capturedTarget;
        private readonly ReactiveEvent<float> _onDamageReceived;

        public PlayerEntity(Ctx ctx)
        {
            _ctx = ctx;
            _movementInput = AddForDisposal(new ReactiveProperty<Vector2>());
            _onDamageableTrigger = AddForDisposal(new ReactiveEvent<List<IDamageable>>());
            _playerVelocity = AddForDisposal(new ReactiveProperty<float>());
            _onGunUsed = AddForDisposal(new ReactiveEvent<IDamageable>());
            _onRocketTargetingStart = AddForDisposal(new ReactiveEvent<List<IDamageable>>());
            _onRocketTargetingStop = AddForDisposal(new ReactiveTrigger());
            _useRockets = AddForDisposal(new ReactiveEvent<List<IDamageable>>());
            _applyDamage = AddForDisposal(new ReactiveEvent<float>());
            _currentHealth = AddForDisposal(new ReactiveProperty<float>());
            _capturedTarget = AddForDisposal(new ReactiveProperty<IDamageable>());
            _onDamageReceived = AddForDisposal(new ReactiveEvent<float>());

            CreateMoverPM();
            CreateAttackerPM();
            CreateHealthPM();
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
            CreateHealthBar(_view.HealthBarView);
        }

        private void CreateHealthBar(HealthBarView healthBarView)
        {
            var ctx = new HealthBarEntity.Ctx()
            {
                CurrentHealth = _currentHealth,
                StartingHealth = _ctx.ContentProvider.Settings.PlayerSettings.Health,
                View = healthBarView
            };
            AddForDisposal(new HealthBarEntity(ctx));
        }

        private void CreateHealthPM()
        {
            var settings = _ctx.ContentProvider.Settings;
            var startingHealth = settings.PlayerSettings.Health;
            var ctx = new HealthPM.Ctx
            {
                CurrentHealth = _currentHealth,
                ApplyDamage = _applyDamage,
                IsAlive = _ctx.IsAlive,
                OnDamageReceived = _onDamageReceived,
                StartingHealth = startingHealth,
                LevelState = _ctx.LevelState
            };
            AddForDisposal(new HealthPM(ctx));
        }

        private void CreateMoverPM()
        {
            var ctx = new PlayerMoverPM.Ctx
            {
                JoystickInput = _ctx.JoystickInput,
                MovementInput = _movementInput,
                LevelState = _ctx.LevelState
            };
            AddForDisposal(new PlayerMoverPM(ctx));
        }

        private void CreateAttackerPM()
        {
            var ctx = new PlayerAttackerPM.Ctx
            {
                OnDamageableTrigger = _onDamageableTrigger,
                OnGunUsed = _onGunUsed,
                OnRocketTargetingStart = _onRocketTargetingStart,
                OnRocketTargetingStop = _onRocketTargetingStop,
                UseRockets = _useRockets,
                PlayerTransform = _ctx.PlayerTransform,
                CapturedTarget = _capturedTarget,
                LevelState = _ctx.LevelState,
                MovementInput = _movementInput,
                PlayerVelocity = _playerVelocity,
                PlayerSettings = _ctx.ContentProvider.Settings.PlayerSettings,
                RocketCooldown = _ctx.RocketCooldown
            };
            AddForDisposal(new PlayerAttackerPM(ctx));
        }

        private async Task CreateViewAsync()
        {
            var asset =_ctx.ContentProvider.PlayerPrefabRef;

            var ctx = new PlayerView.Ctx
            {
                MovementInput = _movementInput,
                PlayerVelocity = _playerVelocity,
                OnDamageableTrigger = _onDamageableTrigger,
                OnGunUsed = _onGunUsed,
                OnRocketTargetingStart = _onRocketTargetingStart,
                OnRocketTargetingStop = _onRocketTargetingStop,
                UseRockets = _useRockets,
                IsAlive = _ctx.IsAlive,
                ApplyDamage = _applyDamage,
                CapturedTarget = _capturedTarget,
                PlayerCameraTarget = _ctx.PlayerCameraTarget,
                OnDamageReceived = _onDamageReceived,

                PlayerSettings = _ctx.ContentProvider.Settings.PlayerSettings,
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot
            };

            _view = AddForDestroy(Object.Instantiate(asset.GetComponent<PlayerView>(),
                _ctx.PlayerConfig.Position, _ctx.PlayerConfig.Rotation));
            _view.SetCtx(ctx);
            _ctx.PlayerTransform.Value = _view.transform;
        }
    }
}
