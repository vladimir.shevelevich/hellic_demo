﻿using App.Game.Level.Common;
using GameData;
using UniRx;
using UnityEngine;

namespace App.Game.Level.Player
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerMoverView : MonoBehaviour
    {
        [SerializeField] private Transform _visual;

        public struct Ctx
        {
            public IReadOnlyReactiveProperty<Vector2> MovementInput;
            public IReadOnlyReactiveProperty<bool> IsAlive;
            public IReadOnlyReactiveProperty<IDamageable> CapturedTarget;
            public ReactiveProperty<Transform> PlayerCameraTarget;
            public PlayerSettings PlayerSettings;
            public ReactiveProperty<float> PlayerCurrentSpeed;
        }

        private Ctx _ctx;

        private CharacterController _characterController;
        private Transform _cameraTargetTransform;
        private Vector3 _velocity;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _characterController = GetComponent<CharacterController>();
            _cameraTargetTransform = new GameObject("CameraTarget").transform;
            _ctx.PlayerCameraTarget.Value = _cameraTargetTransform;

            Observable.EveryUpdate().Subscribe(_ => EveryUpdate()).AddTo(this);
        }

        private void EveryUpdate()
        {
            ChangeVelocity(_ctx.MovementInput.Value);
            Move(_ctx.MovementInput.Value);
            if (_cameraTargetTransform != null)
                MoveCameraTarget();
        }

        private void ChangeVelocity(Vector2 moveInput)
        {
            if (moveInput != Vector2.zero)
            {
                _velocity = new Vector3(moveInput.x, 0, moveInput.y);
                _velocity*= _ctx.PlayerSettings.Speed*Time.deltaTime;
            }
            else
            {
                _velocity = Vector3.Lerp(_velocity, Vector3.zero, 1/_ctx.PlayerSettings.Inertia * Time.deltaTime);
            }
        }

        private void MoveCameraTarget()
        {
            var targetPosition = transform.position + transform.forward * _ctx.PlayerSettings.CameraOffset;
            _cameraTargetTransform.position = Vector3.Lerp(_cameraTargetTransform.position, targetPosition, _ctx.PlayerSettings.CameraSpeed*Time.deltaTime);
        }

        private void Move(Vector2 moveInput)
        {
            var movement = _velocity;
            _characterController.Move(movement);
            ApplyRotation(movement);
            ApplyTilt(moveInput);

            _ctx.PlayerCurrentSpeed.Value = movement.magnitude;
        }

        private void ApplyRotation(Vector3 movement)
        {
            if (movement == Vector3.zero)
                return;

            Quaternion lookRotation;
            float rotationLerpValue;
            if (_ctx.CapturedTarget.Value == null)
            {
                lookRotation = Quaternion.LookRotation(movement);
                rotationLerpValue = _ctx.PlayerSettings.RotationSpeed * Time.deltaTime;
            }
            else
            {
                lookRotation = Quaternion.LookRotation(_ctx.CapturedTarget.Value.TargetTransform.position - transform.position);
                rotationLerpValue = _ctx.PlayerSettings.CapturedTargetRotationSpeed * Time.deltaTime;
            }

            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, rotationLerpValue);
        }

        private void ApplyTilt(Vector2 moveInput)
        {
            var vectorDotHorizontal = Vector3.Dot(new Vector3(moveInput.x, 0, moveInput.y), transform.rotation * Vector3.left);
            var vectorDotVertical = Vector3.Dot(new Vector3(moveInput.x, 0, moveInput.y), transform.rotation * Vector3.forward);
            var targetTilt = new Vector3(-vectorDotVertical * _ctx.PlayerSettings.MaxTiltX, 180, -vectorDotHorizontal * _ctx.PlayerSettings.MaxTiltZ);
            var tiltSpeed = _ctx.PlayerSettings.TiltSpeed;
            Vector3 targetRotationEuler = targetTilt;
            _visual.localRotation = Quaternion.Lerp(_visual.localRotation, Quaternion.Euler(targetRotationEuler), tiltSpeed * Time.deltaTime);
        }

        private void OnDestroy()
        {
            if (_cameraTargetTransform != null)
                Destroy(_cameraTargetTransform.gameObject);
        }
    }
}