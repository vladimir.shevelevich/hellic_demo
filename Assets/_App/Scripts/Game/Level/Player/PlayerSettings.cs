﻿using UnityEngine;

namespace GameData
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Settings/Player")]
    public class PlayerSettings : ScriptableObject
    {
        public int StartMoneyAmount = 0;
        public int Health = 10;
        public bool AttackOutsideView;
        public bool GetAttackOutsideView;

        [Header("Movement")]
        public float Speed = 100;
        public float Inertia = 1;

        public float RotationSpeed = 1;
        public float CapturedTargetRotationSpeed = 1;
        public float MaxTiltX = 20;
        public float MaxTiltZ = 20;
        public float TiltSpeed = 1;

        [Header("Attack")]
        public float AttackRange = 5;
        public float AttackZoneAngle = 20;

        public float GunDamage = 1;
        public float GunCooldown = 1;
        public float GunBulletSpeed = 20;

        [Space]
        public float RocketDamage = 2;

        public float RocketSpeed = 10;
        public float RocketTargetingDuration = 1;
        public float RocketCooldown = 5;


        [Header("Camera")]
        public float CameraSpeed = 5;

        public float CameraOffset = 20;

        public LayerMask PlayerLayer;
        public LayerMask PlayerWeaponLayer;
        public LayerMask EnemyLayer;
        public LayerMask EnemyWeaponLayer;
    }
}