using UnityEngine;

namespace App.Scripts.Game.Level.Player
{
    public class PlayerConstructor : MonoBehaviour
    {
        [SerializeField] private PlayerConfig _config;

        public PlayerConfig CollectConfig()
        {
            _config.Position = transform.position;
            _config.Rotation = transform.rotation;

            return _config;
        }
    }
}
