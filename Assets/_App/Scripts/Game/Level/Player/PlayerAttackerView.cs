﻿using System;
using System.Collections.Generic;
using _App.Scripts.Game.Level.Player.RocketAim;
using App.Game.Level.Common;
using App.Game.Level.Rocket;
using App.GameData;
using DG.Tweening;
using GameData;
using MoreMountains.NiceVibrations;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace _App.Scripts.Game.Level.Player
{
    public class PlayerAttackerView : MonoBehaviour
    {
        public struct Ctx
        {
            public IReadOnlyReactiveEvent<IDamageable> OnGunUsed;
            public IReadOnlyReactiveEvent<List<IDamageable>> OnRocketTargetingStart;
            public IReadOnlyReactiveTrigger OnRocketTargetingStop;
            public IReadOnlyReactiveEvent<List<IDamageable>> UseRockets;
            public IReadOnlyReactiveProperty<int> DamageLevelUpgrade;

            public ReactiveEvent<List<IDamageable>> OnDamageableTrigger;
            public ContentProvider ContentProvider;
            public RectTransform UiRoot;
        }

        private Ctx _ctx;
        private Camera _camera;
        private readonly List<IDamageable> _damageableTriggersCache = new();
        private readonly List<IDisposable> _rocketTargets = new();
        private PlayerSettings PlayerSettings => _ctx.ContentProvider.Settings.PlayerSettings;


        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _camera = Camera.main;
            _ctx.OnGunUsed.SubscribeWithSkip(PlayGunAttack).AddTo(this);
            _ctx.OnRocketTargetingStart.SubscribeWithSkip(CreateRocketAims).AddTo(this);
            _ctx.OnRocketTargetingStop.Subscribe(HideRocketTargets).AddTo(this);
            _ctx.UseRockets.SubscribeWithSkip(SpawnRockets).AddTo(this);

            Observable.EveryUpdate().Subscribe(_ =>EveryUpdate()).AddTo(this);
        }

        private void EveryUpdate()
        {
            ScanDamageable();
        }

        private void PlayGunAttack(IDamageable damageable)
        {
            var targetTransformPosition = damageable.TargetTransform.position;
            var direction = targetTransformPosition - transform.position;
            SpawnBullet(targetTransformPosition);
            PlayBulletHitVfx(direction);
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }

        private void PlayBulletHitVfx(Vector3 direction)
        {
            if (Physics.Raycast(new Ray(transform.position, direction), out var hitInfo, PlayerSettings.EnemyLayer))
            {
                var vfxGO = Instantiate(_ctx.ContentProvider.Vfx.GunHitVfx, hitInfo.point, Quaternion.identity);
                Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(_ => Destroy(vfxGO));
            }
        }

        private void SpawnBullet(Vector3 targetTransformPosition)
        {
            var prefab = _ctx.ContentProvider.GunBulletPrefab;
            var go = Instantiate(prefab, transform.position, Quaternion.identity);
            go.transform.LookAt(targetTransformPosition);
            var distance = Vector3.Distance(targetTransformPosition, transform.position);
            var duration = distance / PlayerSettings.GunBulletSpeed;
            go.transform.DOMove(targetTransformPosition, duration).SetEase(Ease.Linear).OnComplete(() => { Destroy(go); });
        }

        private void CreateRocketAims(List<IDamageable> damageables)
        {
            foreach (var damageable in damageables)
            {
                var ctx = new RocketAimEntity.Ctx
                {
                    TargetTransform = damageable.TargetTransform,
                    TargetingDuration =_ctx.ContentProvider.Settings.PlayerSettings.RocketTargetingDuration,
                    UiRoot = _ctx.UiRoot,
                    RocketAimPrefab = _ctx.ContentProvider.RocketAimPrefab
                };
                _rocketTargets.Add(new RocketAimEntity(ctx).AddTo(this));
            }
        }

        private void HideRocketTargets()
        {
            foreach (var rocketTarget in _rocketTargets)
                rocketTarget?.Dispose();

            _rocketTargets.Clear();
        }

        private void SpawnRockets(List<IDamageable> damageables)
        {
            foreach (var damageable in damageables)
            {
                var damage = PlayerSettings.RocketDamage;
                var ctx = new RocketEntity.Ctx
                {
                    RocketPrefab = _ctx.ContentProvider.PlayerRocketPrefab,
                    TargetPosition = damageable.TargetTransform.position,
                    RocketDamage = damage,
                    RocketSpeed = _ctx.ContentProvider.Settings.PlayerSettings.RocketSpeed,
                    RocketStartPosition = transform.position,
                    ContentProvider = _ctx.ContentProvider
                };
                var entity = new RocketEntity(ctx).AddTo(this);
                Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(_ => entity.Dispose()).AddTo(this);
            }
        }

        readonly Collider[] hitsResult = new Collider[50];

        private void ScanDamageable()
        {
            int hitsCount = Physics.OverlapSphereNonAlloc(transform.position, PlayerSettings.AttackRange, hitsResult, PlayerSettings.EnemyLayer);
            _damageableTriggersCache.Clear();
            for (int i = 0; i < hitsCount; i++)
            {
                if (hitsResult[i].TryGetComponent(out IDamageable damageable))
                {
                    if (!damageable.IsAlive)
                        continue;
                    if (!PlayerSettings.AttackOutsideView && !IsDamageableInsideView(damageable))
                        continue;

                    var angle = Vector3.Angle(transform.forward, damageable.TargetTransform.position - transform.position);
                    if (angle <= PlayerSettings.AttackZoneAngle/2)
                        _damageableTriggersCache.Add(damageable);
                }
            }
            if (_damageableTriggersCache.Count > 0)
                _ctx.OnDamageableTrigger.Invoke(_damageableTriggersCache);
        }

        private bool IsDamageableInsideView(IDamageable damageable)
        {
            var viewportPosition = _camera.WorldToViewportPoint(damageable.TargetTransform.position);
            return viewportPosition.x >= 0 && viewportPosition.x <= 1 && viewportPosition.y >= 0 &&
                   viewportPosition.y <= 1;
        }

        private void OnDrawGizmos()
        {
            if (_ctx.ContentProvider == null)
                return;

            Gizmos.color = Color.red;;
            Gizmos.DrawWireSphere(transform.position, PlayerSettings.AttackRange);
        }
    }
}