﻿using System;
using App.Scripts.Game.Level.Enemy;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _App.Scripts.Game.Level.Enemy.Mover
{
    public class EnemyPatrolMoverView : EnemyMoverView
    {
        private int _currentDirection = 1;
        private Vector3 _initialPosition;
        private Vector3 _initialForward;

        protected override void OnContextSet()
        {
            _initialPosition = transform.position;
            _initialForward = transform.forward;

            if (ctx.MovingEnabled.Value)
                MoveToPatrolPoint();
            ctx.MovingEnabled.Subscribe(moving =>
            {
                if (moving)
                    MoveToPatrolPoint();
                else
                    StopMoving();
            }).AddTo(this);
            MoveToPatrolPoint();
        }

        private void MoveToPatrolPoint()
        {
            if (ctx.EnemyConfig.MovementLenght <= 0)
                return;

            var patrolPoint = _initialPosition + _initialForward * ctx.EnemyConfig.MovementLenght/2 * _currentDirection;
            transform.DOLookAt(patrolPoint, 0.1f);
            var distance = Vector3.Distance(patrolPoint, transform.position);
            transform.DOMove(patrolPoint, distance/ctx.EnemyConfig.MovementSpeed).SetEase(Ease.Linear).OnComplete(OnMoveComplete);
            ctx.IsMoving.Value = true;
        }

        private void StopMoving()
        {
            Debug.Log("Stop MOVING");
            transform.DOKill();
            ctx.IsMoving.Value = false;
        }

        private void OnMoveComplete()
        {
            ctx.IsMoving.Value = false;
            _currentDirection *= -1;
            Observable.Timer(TimeSpan.FromSeconds(ctx.EnemyConfig.MovementInterval)).Subscribe(_ =>
            {
                if (ctx.MovingEnabled.Value)
                    MoveToPatrolPoint();
            }).AddTo(this);
        }
    }
}