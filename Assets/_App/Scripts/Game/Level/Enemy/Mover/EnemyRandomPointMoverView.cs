﻿using System;
using App.Scripts.Game.Level.Enemy;
using DG.Tweening;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace App.Game.Level.Enemy
{
    public class EnemyRandomPointMoverView : EnemyMoverView
    {
        private Vector3 _initialPosition;

        protected override void OnContextSet()
        {
            _initialPosition = transform.position;

            ctx.MovingEnabled.Subscribe(moving =>
            {
                if (moving)
                    MoveToRandomPoint();
                else
                    StopMoving();
            }).AddTo(this);
        }

        private void MoveToRandomPoint()
        {
            if (ctx.EnemyConfig.MovementLenght <= 0)
                return;

            var randomPointInCircle = Random.insideUnitCircle;
            var position = _initialPosition + new Vector3(randomPointInCircle.x, 0, randomPointInCircle.y)*ctx.EnemyConfig.MovementLenght;
            transform.DOLookAt(position, 0.1f);
            var distance = Vector3.Distance(position, transform.position);
            transform.DOMove(position, distance/ctx.EnemyConfig.MovementSpeed).SetEase(Ease.Linear).OnComplete(OnMoveComplete);
            ctx.IsMoving.Value = true;
        }

        private void StopMoving()
        {
            transform.DOKill();
            ctx.IsMoving.Value = false;
        }

        private void OnMoveComplete()
        {
            ctx.IsMoving.Value = false;
            Observable.Timer(TimeSpan.FromSeconds(ctx.EnemyConfig.MovementInterval)).Subscribe(_ =>
            {
                if (ctx.MovingEnabled.Value)
                    MoveToRandomPoint();
            }).AddTo(this);
        }
    }
}