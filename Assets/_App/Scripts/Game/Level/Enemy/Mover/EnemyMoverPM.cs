﻿using System;
using App.Game.Level;
using App.Game.Level.Common;
using App.Scripts.Game.Level.Enemy;
using Protopunk.Tools;
using UniRx;

namespace _App.Scripts.Game.Level.Enemy.Mover
{
    public class EnemyMoverPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<bool> IsAlive;
            public IReadOnlyReactiveEvent<IDamageable> UseRocket;
            public ReactiveProperty<bool> MovingEnabled;
            public EnemyConfig EnemyConfig;
            public IReadOnlyReactiveProperty<LevelState> LevelState;
        }

        private readonly Ctx _ctx;
        private bool _isStoppedOnAttack;

        public EnemyMoverPM(Ctx ctx)
        {
            _ctx = ctx;

            _ctx.MovingEnabled.Value = true;
            AddForDisposal(_ctx.UseRocket.SubscribeWithSkip(_ => StopOnAttack()));
            AddForDisposal(_ctx.IsAlive.Skip(1).Subscribe(isAlive => UpdateMovingEnabled()));
            AddForDisposal(_ctx.LevelState.Subscribe(_ => UpdateMovingEnabled()));
        }

        private void StopOnAttack()
        {
            _isStoppedOnAttack = true;
            UpdateMovingEnabled();
            
            AddForDisposal(Observable.Timer(TimeSpan.FromSeconds(_ctx.EnemyConfig.AttackStopDuration)).Subscribe(_ =>
            {
                _isStoppedOnAttack = false;
                UpdateMovingEnabled();
            }));
        }

        private void UpdateMovingEnabled() => 
            _ctx.MovingEnabled.Value = _ctx.IsAlive.Value && _ctx.LevelState.Value == LevelState.InProcess && !_isStoppedOnAttack;
    }
}