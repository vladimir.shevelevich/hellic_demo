﻿using _App.Scripts.Game.Level.Enemy;
using GameData;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.Enemy
{
    public abstract class EnemyMoverView : MonoBehaviour, IEnemyMover
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<bool> MovingEnabled;
            public ReactiveProperty<bool> IsMoving;
            public EnemyConfig EnemyConfig;
        }

        protected Ctx ctx;

        public void SetCtx(Ctx ctxToSet)
        {
            ctx = ctxToSet;
            OnContextSet();
        }

        protected abstract void OnContextSet();
    }

    public interface IEnemyMover {}
}