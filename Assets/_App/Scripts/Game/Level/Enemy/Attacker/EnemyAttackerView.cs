﻿using System;
using App.Game.Enums;
using App.Game.Level.Common;
using App.Game.Level.Rocket;
using App.GameData;
using App.Scripts.Game.Level.Enemy;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace _App.Scripts.Game.Level.Enemy
{
    public abstract class EnemyAttackerView : MonoBehaviour, IEnemyAttacker
    {
        [SerializeField] private LayerMask _playerLayerMask;

        public struct Ctx
        {
            public IReadOnlyReactiveEvent<IDamageable> UseRocket;
            public ReactiveEvent<IDamageable> OnPlayerTrigger;
            public EnemyConfig EnemyConfig;
            public ContentProvider ContentProvider;
        }

        private Ctx _ctx;

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;
            _ctx.UseRocket.SubscribeWithSkip(UseRocket).AddTo(this);
            Observable.EveryUpdate().Subscribe(_ => EveryUpdate()).AddTo(this);
        }

        private void EveryUpdate()
        {
            ScanForPlayer();
        }

        private void UseRocket(IDamageable playerDamageable)
        {
            int amountOfRockets = 0;
            switch (_ctx.EnemyConfig.AttackType)
            {
                case EnemyAttackType.Single:
                    amountOfRockets = 1;
                    break;
                case EnemyAttackType.Double:
                    amountOfRockets = 2;
                    break;
                case EnemyAttackType.Triple:
                    amountOfRockets = 3;
                    break;
            }

            var targetPosition = playerDamageable.TargetTransform.position;
            LookAtTarget(targetPosition);
            for (var i = 0; i < amountOfRockets; i++)
            {
                Observable.Timer(TimeSpan.FromSeconds(_ctx.EnemyConfig.MultipleAttackInterval*i)).Subscribe(_ =>
                    SpawnRocket(targetPosition)).AddTo(this);
            }
        }

        readonly Collider[] hitsResult = new Collider[50];
        private void ScanForPlayer()
        {
            int hitsCount = Physics.OverlapSphereNonAlloc(transform.position, _ctx.EnemyConfig.AttackRange, hitsResult, _playerLayerMask);
            for (int i = 0; i < hitsCount; i++)
            {
                if (hitsResult[i].TryGetComponent(out IDamageable damageable))
                {
                    if (damageable.IsAlive)
                        _ctx.OnPlayerTrigger.Invoke(damageable);
                    return;
                }
            }
        }

        private void SpawnRocket(Vector3 targetPosition)
        {
            var ctx = new RocketEntity.Ctx
            {
                RocketPrefab = _ctx.ContentProvider.EnemyRocketPrefab,
                TargetPosition = targetPosition,
                RocketDamage = _ctx.EnemyConfig.AttackDamage,
                RocketSpeed = _ctx.EnemyConfig.RocketSpeed,
                RocketStartPosition = transform.position,
                ContentProvider = _ctx.ContentProvider
            };
            var entity = new RocketEntity(ctx).AddTo(this);
            Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(_ => entity.Dispose()).AddTo(this);
        }

        protected abstract void LookAtTarget(Vector3 targetPosition);

        private void OnDrawGizmos()
        {
            if (_ctx.EnemyConfig == null)
                return;

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _ctx.EnemyConfig.AttackRange);
        }
    }

    public interface IEnemyAttacker { }
}