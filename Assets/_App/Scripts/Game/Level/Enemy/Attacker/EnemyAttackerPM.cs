﻿using App.Game.Level;
using App.Game.Level.Common;
using App.Game.Level.Enemy;
using GameData;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Level.Enemy
{
    public class EnemyAttackerPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveEvent<IDamageable> OnPlayerTrigger;
            public ReactiveEvent<IDamageable> UseRocket;
            public IReadOnlyReactiveProperty<bool> IsAlive;
            public IReadOnlyReactiveProperty<LevelState> LevelState;
            public PlayerSettings PlayerSettings;

            public EnemyConfig EnemyConfig;
        }

        private readonly Ctx _ctx;
        private EnemyView _view;

        private float _lastRocketUseTime = float.MinValue;
        private readonly UnityEngine.Camera _camera;

        public EnemyAttackerPM(Ctx ctx)
        {
            _ctx = ctx;
            _camera = UnityEngine.Camera.main;
            AddForDisposal(_ctx.OnPlayerTrigger.SubscribeWithSkip(OnPlayerTrigger));
        }

        public void BindView(EnemyView view)
        {
            _view = view;
        }

        private void OnPlayerTrigger(IDamageable playerDamageable)
        {
            if (!CanAttack())
                return;

            if (!_ctx.PlayerSettings.GetAttackOutsideView && !IsInsideView())
                return;

            if (IsRocketsAvailable())
                UseRockets(playerDamageable);
        }
        
        private bool CanAttack() => 
            _ctx.IsAlive.Value && _ctx.LevelState.Value == LevelState.InProcess;

        private void UseRockets(IDamageable playerDamageable)
        {
            _ctx.UseRocket.Invoke(playerDamageable);
            _lastRocketUseTime = Time.realtimeSinceStartup;
        }

        private bool IsRocketsAvailable()
        {
            return Time.realtimeSinceStartup - _lastRocketUseTime > _ctx.EnemyConfig.AttackCooldown;
        }

        private bool IsInsideView()
        {
            if (_view == null)
                return false;

            var viewportPosition = _camera.WorldToViewportPoint(_view.gameObject.transform.position);
            return viewportPosition.x >= 0 && viewportPosition.x <= 1 && viewportPosition.y >= 0 &&
                   viewportPosition.y <= 1;
        }
    }
}