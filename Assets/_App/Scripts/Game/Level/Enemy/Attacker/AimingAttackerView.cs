﻿using DG.Tweening;
using UnityEngine;

namespace _App.Scripts.Game.Level.Enemy.Attacker
{
    public class AimingAttackerView : EnemyAttackerView
    {
        [SerializeField] private Transform _gun;

        protected override void LookAtTarget(Vector3 targetPosition)
        {
            _gun.DOLookAt(targetPosition, 0.1f);
        }
    }
}