﻿using DG.Tweening;
using UnityEngine;

namespace _App.Scripts.Game.Level.Enemy.Attacker
{
    public class TrooperAttackerView : EnemyAttackerView
    {
        protected override void LookAtTarget(Vector3 targetPosition)
        {
            transform.DOLookAt(targetPosition, 0.1f);
        }
    }
}