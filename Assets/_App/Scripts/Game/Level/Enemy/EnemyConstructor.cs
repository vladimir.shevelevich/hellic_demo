using UnityEngine;

namespace App.Scripts.Game.Level.Enemy
{
    public class EnemyConstructor : MonoBehaviour
    {
        [SerializeField] private EnemyConfig _config;
        
        public EnemyConfig CollectConfig()
        {
            _config.Position = transform.position;
            _config.Rotation = transform.rotation;
            
            return _config;
        }
    }
}