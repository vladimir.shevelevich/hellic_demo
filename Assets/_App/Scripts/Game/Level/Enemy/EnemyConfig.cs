﻿using System;
using App.Game.Enums;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using UnityEngine;

namespace App.Scripts.Game.Level.Enemy
{
    [Serializable]
    public class EnemyConfig
    {
        public Vector3 Position;
        public Quaternion Rotation;
        
        public EnemyType EnemyType;
        public int Health = 5;

        [Header("Attack")]
        public float AttackRange = 5;
        public float RocketSpeed;
        public EnemyAttackType AttackType;
        public int AttackDamage = 1;
        public float AttackCooldown = 5;
        public float MultipleAttackInterval = 0.4f;

        [Header("Movement")]
        public float MovementSpeed = 1;
        public float MovementLenght = 1;
        public float MovementInterval = 1;
        public float AttackStopDuration = 1;
    }
}