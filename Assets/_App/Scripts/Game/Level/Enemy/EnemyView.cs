﻿using System;
using _App.Scripts.Game.Level.Enemy;
using App.Game.Enums;
using App.Scripts.Game.Level.Enemy;
using App.Game.Level.Common;
using App.GameData;
using App.Scripts.Game.Level.HealthBar;
using MoreMountains.NiceVibrations;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game.Level.Enemy
{
    public class EnemyView : MonoBehaviour, IDamageable
    {
        public HealthBarView HealthBarView => _healthBarView;
        
        [SerializeField] private GameObject _visual;
        [SerializeField] private Transform _damageableTarget;
        [SerializeField] private HealthBarView _healthBarView;
        [SerializeField] private Animator _animator;

        public struct Ctx
        {
            public ReactiveEvent<float> ApplyDamage;
            public IReadOnlyReactiveEvent<IDamageable> UseRocket;
            public IReadOnlyReactiveProperty<bool> IsAlive;
            public IReadOnlyReactiveProperty<bool> MovingEnabled;
            public ReactiveEvent<IDamageable> OnPlayerTrigger;
            public ReactiveProperty<bool> IsMoving;
            public IReadOnlyReactiveEvent<float> OnDamageReceived;

            public EnemyConfig EnemyConfig;
            public ContentProvider ContentProvider;
        }

        private Ctx _ctx;

        public Animator Animator => _animator;
        bool IDamageable.IsAlive => _ctx.IsAlive.Value;
        Transform IDamageable.TargetTransform => _damageableTarget;
        int IDamageable.AttackPriority => GetAttackPriority();

        public void SetCtx(Ctx ctx)
        {
            _ctx = ctx;

            InitMover();
            InitAttacker();
            InitDamageNumberSpawner();

            _ctx.IsAlive.Subscribe(isAlive =>
            {
                if (!isAlive)
                    PlayDeath();
            }).AddTo(this);
        }

        private void InitDamageNumberSpawner()
        {
            var ctx = new DamageNumberSpawnerView.Ctx
            {
                ContentProvider = _ctx.ContentProvider,
                SpawnDamageNumber = _ctx.OnDamageReceived
            };

            if (TryGetComponent<DamageNumberSpawnerView>(out var spawner))
            {
                spawner.SetCtx(ctx);
            }
        }

        private void InitMover()
        {
            var ctx = new EnemyMoverView.Ctx
            {
                MovingEnabled = _ctx.MovingEnabled,
                IsMoving = _ctx.IsMoving,
                EnemyConfig = _ctx.EnemyConfig
            };
            var mover = GetComponent<IEnemyMover>();
            if (mover != null)
                ((EnemyMoverView)mover).SetCtx(ctx);
        }

        private void InitAttacker()
        {
            var ctx = new EnemyAttackerView.Ctx
            {
                UseRocket = _ctx.UseRocket,
                OnPlayerTrigger = _ctx.OnPlayerTrigger,
                EnemyConfig = _ctx.EnemyConfig,
                ContentProvider = _ctx.ContentProvider
            };
            var attacker = GetComponent<IEnemyAttacker>();
            if (attacker != null)
                ((EnemyAttackerView)attacker).SetCtx(ctx);
        }

        void IDamageable.ApplyDamage(float damage)
        {
            _ctx.ApplyDamage.Invoke(damage);
        }

        private void PlayDeath()
        {
            Observable.Timer(TimeSpan.FromSeconds(2)).Subscribe(_ => _visual.SetActive(false)).AddTo(this);
            GetComponent<Collider>().enabled = false;
            if (_ctx.EnemyConfig.EnemyType != EnemyType.Footman)
                MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
        }

        private int GetAttackPriority()
        {
            var attackPrioritySettings = _ctx.ContentProvider.Settings.AttackPrioritySettings;
            switch (_ctx.EnemyConfig.EnemyType)
            {
                case EnemyType.Footman:
                case EnemyType.Jeep:
                case EnemyType.Tank:
                case EnemyType.Tower:
                    return attackPrioritySettings.EnemyPriority;
                case EnemyType.BarrelProps:
                    return attackPrioritySettings.BarrelPriority;
                case EnemyType.TowerProps:
                case EnemyType.SmallBuildingProps:
                case EnemyType.MediumBuildingProps:
                    return attackPrioritySettings.DestroyablePropPriority;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}