﻿using App.Game.Level.Common;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace _App.Scripts.Game.Level.Enemy
{
    public class EnemyAnimatorPM : BaseDisposable
    {
        public struct Ctx
        {
            public IReadOnlyReactiveProperty<bool> IsMoving;
            public IReadOnlyReactiveEvent<IDamageable> UseRockets;
            public IReadOnlyReactiveProperty<bool> IsAlive;
            public Animator Animator;
        }

        private readonly Ctx _ctx;
        private static readonly int MovingHash = Animator.StringToHash("Moving");
        private static readonly int AttackHash = Animator.StringToHash("Attack");
        private static readonly int DeathHash = Animator.StringToHash("Death");

        public EnemyAnimatorPM(Ctx ctx)
        {
            _ctx = ctx;
            AddForDisposal(_ctx.IsMoving.Subscribe(moving => {_ctx.Animator.SetBool(MovingHash, moving);}));
            AddForDisposal(_ctx.UseRockets.SubscribeWithSkip(_ => _ctx.Animator.SetTrigger(AttackHash)));
            AddForDisposal(_ctx.IsAlive.Skip(1).Subscribe(isAlive =>
            {
                if (!isAlive)
                    _ctx.Animator.SetTrigger(DeathHash);
            }));
            _ctx.Animator.SetBool(MovingHash, _ctx.IsMoving.Value);
        }
    }
}