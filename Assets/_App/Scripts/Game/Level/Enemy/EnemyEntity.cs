﻿using System.Threading.Tasks;
using _App.Scripts.Game.Level.Enemy;
using _App.Scripts.Game.Level.Enemy.Mover;
using App.Scripts.Game.Level.Enemy;
using App.Game.Level.Common;
using App.GameData;
using App.Scripts.Game.Level.HealthBar;
using Protopunk.Tools;
using UniRx;
using UnityEngine;
using Object = UnityEngine.Object;

namespace App.Game.Level.Enemy
{
    public class EnemyEntity : BaseDisposable
    {
        public struct Ctx
        {
            public ContentProvider ContentProvider;
            public EnemyConfig EnemyConfig;
            public IReadOnlyReactiveProperty<LevelState> LevelState;
        }

        private readonly Ctx _ctx;
        private EnemyView _view;
        private EnemyAttackerPM _attackerPm;

        private readonly ReactiveEvent<float> _applyDamage;
        private readonly ReactiveProperty<float> _currentHealth;
        private readonly ReactiveProperty<bool> _isAlive;
        private readonly ReactiveEvent<IDamageable> _useRocket;
        private readonly ReactiveEvent<IDamageable> _onPlayerTrigger;
        private readonly ReactiveProperty<bool> _movingEnabled;
        private readonly ReactiveProperty<bool> _isMoving;
        private readonly ReactiveEvent<float> _onDamageReceived;

        public EnemyEntity(Ctx ctx)
        {
            _ctx = ctx;
            _applyDamage = AddForDisposal(new ReactiveEvent<float>());
            _currentHealth = AddForDisposal(new ReactiveProperty<float>());
            _isAlive = AddForDisposal(new ReactiveProperty<bool>());
            _useRocket = AddForDisposal(new ReactiveEvent<IDamageable>());
            _onPlayerTrigger = AddForDisposal(new ReactiveEvent<IDamageable>());
            _movingEnabled = AddForDisposal(new ReactiveProperty<bool>());
            _isMoving = AddForDisposal(new ReactiveProperty<bool>());
            _onDamageReceived = AddForDisposal(new ReactiveEvent<float>());

            CreateHealthPM();
            _attackerPm = CreateAttackerPM();
            CreateMoverPM();
        }

        public async Task InitializeAsync()
        {
            await CreateViewAsync();
            CreateHealthBar();
        }
        
        private void CreateHealthBar()
        {
            if (_view == null || _view.HealthBarView == null)
                return;
            
            var ctx = new HealthBarEntity.Ctx()
            {
                CurrentHealth = _currentHealth,
                StartingHealth = _ctx.EnemyConfig.Health,
                View = _view.HealthBarView
            };
            AddForDisposal(new HealthBarEntity(ctx));
        }

        private void CreateHealthPM()
        {
            var ctx = new HealthPM.Ctx
            {
                CurrentHealth = _currentHealth,
                ApplyDamage = _applyDamage,
                IsAlive = _isAlive,
                OnDamageReceived = _onDamageReceived,
                StartingHealth = _ctx.EnemyConfig.Health,
                LevelState = _ctx.LevelState
            };
            AddForDisposal(new HealthPM(ctx));
        }

        private EnemyAttackerPM CreateAttackerPM()
        {
            var ctx = new EnemyAttackerPM.Ctx
            {
                OnPlayerTrigger = _onPlayerTrigger,
                UseRocket = _useRocket,
                IsAlive = _isAlive,
                EnemyConfig = _ctx.EnemyConfig,
                PlayerSettings = _ctx.ContentProvider.Settings.PlayerSettings,
                LevelState = _ctx.LevelState
            };
            return AddForDisposal(new EnemyAttackerPM(ctx));
        }

        private void CreateMoverPM()
        {
            var ctx = new EnemyMoverPM.Ctx
            {
                IsAlive = _isAlive,
                UseRocket = _useRocket,
                MovingEnabled = _movingEnabled,
                EnemyConfig = _ctx.EnemyConfig,
                LevelState = _ctx.LevelState
            };
            AddForDisposal(new EnemyMoverPM(ctx));
        }

        private void CreateAnimatorPM()
        {
            var ctx = new EnemyAnimatorPM.Ctx
            {
                IsMoving = _isMoving,
                UseRockets = _useRocket,
                IsAlive = _isAlive,
                Animator = _view.Animator
            };
            AddForDisposal(new EnemyAnimatorPM(ctx));
        }

        private async Task CreateViewAsync()
        {
            var prefabRef = _ctx.ContentProvider.Enemies.PrefabRefByType(_ctx.EnemyConfig.EnemyType);
            var asset = prefabRef;

            var ctx = new EnemyView.Ctx
            {
                ApplyDamage = _applyDamage,
                IsAlive = _isAlive,
                UseRocket = _useRocket,
                MovingEnabled = _movingEnabled,
                IsMoving = _isMoving,
                OnPlayerTrigger = _onPlayerTrigger,
                EnemyConfig = _ctx.EnemyConfig,
                ContentProvider = _ctx.ContentProvider,
                OnDamageReceived = _onDamageReceived
            };

            _view = AddForDestroy(Object.Instantiate(asset.GetComponent<EnemyView>(),
                _ctx.EnemyConfig.Position, _ctx.EnemyConfig.Rotation));
            _view.SetCtx(ctx);

            OnViewLoaded();
        }

        private void OnViewLoaded()
        {
            if (_view.Animator != null)
                CreateAnimatorPM();
            _attackerPm.BindView(_view);
        }
    }
}