﻿using App.Game.Level;
using App.GameData;
using App.Scripts.Game.Save;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game
{
    public class GamePM : BaseDisposable
    {
        public struct Ctx
        {
            public ReactiveTrigger SaveGameTrigger;
            public IReadOnlyReactiveEvent<GameSaveData> CollectSaveData;
            public GameSaveData GameSaveData;
            
            public ContentProvider ContentProvider;
            public ReactiveEvent<LevelConfig> StartLevel;
            public IReadOnlyReactiveTrigger Finished;
            public IReadOnlyReactiveTrigger RestartLevel;
            public ReactiveProperty<int> LevelIndex;
            public ReactiveProperty<int> MoneyAmount;
        }

        private readonly Ctx _ctx;

        public GamePM(Ctx ctx)
        {
            _ctx = ctx;

            AddForDisposal(_ctx.Finished.Subscribe(FinishLevel));
            AddForDisposal(_ctx.RestartLevel.Subscribe(RestartLevel));
            AddForDisposal(_ctx.CollectSaveData.SubscribeWithSkip(CollectSaveData));

            if (_ctx.GameSaveData == null)
                SetInitialValues();
            else
                ApplySaveData(_ctx.GameSaveData);

            StartLevel(_ctx.LevelIndex.Value);
        }

        private void SetInitialValues()
        {
            _ctx.MoneyAmount.Value = _ctx.ContentProvider.Settings.PlayerSettings.StartMoneyAmount;
            _ctx.LevelIndex.Value = 0;
        }
        
        private void ApplySaveData(GameSaveData saveData)
        {
            _ctx.LevelIndex.Value = saveData.LevelIndex;
            _ctx.MoneyAmount.Value = saveData.MoneyAmount;
        }

        private void CollectSaveData(GameSaveData saveData)
        {
            saveData.Version = Application.version;
            saveData.LevelIndex = _ctx.LevelIndex.Value;
            saveData.MoneyAmount = _ctx.MoneyAmount.Value;
        }

        private void RestartLevel() => 
            StartLevel(_ctx.LevelIndex.Value);

        private void FinishLevel()
        {
            _ctx.LevelIndex.Value++;
            LoadUpgradeScene();
        }

        private void StartLevel(int levelIndex)
        {
            var realLevelIndex = levelIndex % _ctx.ContentProvider.LevelConfigs.Length;
            var levelConfig = _ctx.ContentProvider.LevelConfigs[realLevelIndex];
            _ctx.StartLevel.Invoke(levelConfig);
            _ctx.SaveGameTrigger.Invoke();
        }

        private void LoadUpgradeScene()
        {
            _ctx.SaveGameTrigger.Invoke();
        }
    }
}