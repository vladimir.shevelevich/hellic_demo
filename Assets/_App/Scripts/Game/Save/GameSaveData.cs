﻿using System;

namespace App.Scripts.Game.Save
{
    [Serializable]
    public class GameSaveData
    {
        public string Version;
        
        public int LevelIndex;
        public int MoneyAmount;
        public int MoneyUpgradeLevel;
        public int DamageUpgradeLevel;
        public int HealthUpgradeLevel;
    }
}