﻿using System;
using System.Collections.Generic;
using App.Scripts.Game.Save.VersionMigrators;
using UnityEngine;

namespace App.Scripts.Game.Save
{
    public static class SaveMigration
    {
        private static string CurrentVersion => Application.version;

        private static readonly Dictionary<string, IVersionMigrator> _versionMigrators = new Dictionary<string, IVersionMigrator>
        {
            {"null", new VersionMigrator_Null()}
        };

        public static void MigrateToCurrentVersion(GameSaveData gameSaveData)
        {
            if (string.IsNullOrEmpty(gameSaveData.Version))
                gameSaveData.Version = "null";

            while (!string.Equals(gameSaveData.Version, CurrentVersion))
            {
                if (_versionMigrators.TryGetValue(gameSaveData.Version, out var versionMigrator))
                {
                    string oldVersion = gameSaveData.Version;
                    versionMigrator.MigrateToNextVersion(gameSaveData);
                    Debug.Log($"Save data was migrated from version {oldVersion} to version {gameSaveData.Version}");
                }
                else
                {
                    throw new Exception($"Migrator to version {CurrentVersion} has not been found");
                }
            }
        }


    }
}