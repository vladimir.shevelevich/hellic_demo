﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Protopunk.Tools;
using UnityEngine;

namespace App.Scripts.Common
{
    public abstract class BaseSavePM<TSaveData> : BaseDisposable
    {
        protected async Task<TSaveData> LoadDataAsync()
        {
            if (!File.Exists(SaveDataPath))
                return default;

            using var streamReader = new StreamReader(SaveDataPath);
            var json = await streamReader.ReadToEndAsync();
            return await Task.Run( () => JsonConvert.DeserializeObject<TSaveData>(json));
        }

        protected async Task<bool> SaveDataAsync(TSaveData dataToSave)
        {
            var json = await Task.Run( () => JsonConvert.SerializeObject(dataToSave));
            try
            {
                using var streamWriter = new StreamWriter(SaveDataPath, false);
                await streamWriter.WriteLineAsync(json);
                await streamWriter.FlushAsync();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        protected abstract string SaveKey { get; }
        private string SaveDataPath => Path.Combine(Application.persistentDataPath, $"{SaveKey}.sv");
    }
}