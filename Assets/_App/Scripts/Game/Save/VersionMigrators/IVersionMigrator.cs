﻿namespace App.Scripts.Game.Save.VersionMigrators
{
    public interface IVersionMigrator
    {
        void MigrateToNextVersion(GameSaveData gameSaveData);
    }
}