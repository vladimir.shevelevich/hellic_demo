﻿using UnityEngine;

namespace App.Scripts.Game.Save.VersionMigrators
{
    public class VersionMigrator_Null : IVersionMigrator
    {
        private static string NextVersion => Application.version;

        public void MigrateToNextVersion(GameSaveData gameSaveData)
        {
            gameSaveData.Version = NextVersion;
        }
    }
}