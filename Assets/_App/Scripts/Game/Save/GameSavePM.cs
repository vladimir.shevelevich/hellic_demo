﻿using System;
using System.Threading.Tasks;
using App.Scripts.Common;
using App.Scripts.Game.Save;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Scripts.Game.Save
{
    public class GameSavePM : BaseSavePM<GameSaveData>
    {
        protected override string SaveKey => "GameSaveData";
        
        private const int AutoSavingInterval = 5;

        public struct Ctx
        {
            public IReadOnlyReactiveTrigger SaveGameTrigger;
            public ReactiveEvent<GameSaveData> CollectSaveData;
            public ReactiveProperty<GameSaveData> GameSaveData;
        }

        private readonly Ctx _ctx;

        public GameSavePM(Ctx ctx)
        {
            _ctx = ctx;

            AddForDisposal(_ctx.SaveGameTrigger.Subscribe(OnSaveGameTriggerInvoked));
            AddForDisposal(AutoSaving());
            AddForDisposal(Observable.OnceApplicationQuit().Subscribe(_ =>
            {
                SaveGameDataAsync();
            }));
        }

        public async Task InitializeAsync()
        {
            await LoadGameDataAsync();
        }

        private void OnSaveGameTriggerInvoked()
        {
            SaveGameDataAsync();
        }

        private async Task SaveGameDataAsync()
        {
            var gameSaveData = new GameSaveData();
            _ctx.CollectSaveData.Invoke(gameSaveData);

            var success = await SaveDataAsync(gameSaveData);
        }

        private async Task LoadGameDataAsync()
        {
            var gameSaveData = await LoadDataAsync();
            if (gameSaveData != null)
                SaveMigration.MigrateToCurrentVersion(gameSaveData);

            _ctx.GameSaveData.Value = gameSaveData;
        }

        private IDisposable AutoSaving()
        {
            return AddForDisposal(Observable.Interval(TimeSpan.FromSeconds(AutoSavingInterval)).Subscribe(_ =>
            {
                SaveGameDataAsync();
            }));
        }
    }
}