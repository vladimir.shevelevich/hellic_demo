﻿using System;
using App.Game.Level;
using App.GameData;
using App.Scripts.Game.Save;
using Protopunk.Tools;
using UniRx;
using UnityEngine;

namespace App.Game
{
    public class GameEntity : BaseDisposable
    {
        public struct Ctx
        {
            public RectTransform UiRoot;
            public ContentProvider ContentProvider;
        }

        private readonly Ctx _ctx;
        private readonly ReactiveProperty<int> _levelIndex;
        private readonly ReactiveTrigger _levelFinished;
        private readonly ReactiveTrigger _restartLevel;
        private readonly ReactiveEvent<LevelConfig> _startLevel;
        private readonly ReactiveTrigger _saveGameTrigger;
        private readonly ReactiveEvent<GameSaveData> _collectSaveData;
        private readonly ReactiveProperty<GameSaveData> _gameSaveData;
        private readonly ReactiveProperty<int> _moneyAmount;

        private IDisposable _currentLevelEntity;

        public GameEntity(Ctx ctx)
        {
            _ctx = ctx;

            _levelFinished = AddForDisposal(new ReactiveTrigger());
            _levelIndex = AddForDisposal(new ReactiveProperty<int>());
            _restartLevel = AddForDisposal(new ReactiveTrigger());
            _startLevel = AddForDisposal(new ReactiveEvent<LevelConfig>());
            _collectSaveData = AddForDisposal(new ReactiveEvent<GameSaveData>());
            _gameSaveData = AddForDisposal(new ReactiveProperty<GameSaveData>());
            _saveGameTrigger = AddForDisposal(new ReactiveTrigger());
            _moneyAmount = AddForDisposal(new ReactiveProperty<int>());

            AddForDisposal(_startLevel.SubscribeWithSkip(OnStartLevel));
            
            BuildGameAsync();
        }

        private async void BuildGameAsync()
        {
            await CreateGameSavePM().InitializeAsync();
            CreateGamePM();
        }

        private GameSavePM CreateGameSavePM()
        {
            var ctx = new GameSavePM.Ctx()
            {
                SaveGameTrigger = _saveGameTrigger,
                CollectSaveData = _collectSaveData,
                GameSaveData = _gameSaveData
            };
            return AddForDisposal(new GameSavePM(ctx));
        }

        private void CreateGamePM()
        {
            var ctx = new GamePM.Ctx()
            {
                ContentProvider = _ctx.ContentProvider,
                Finished = _levelFinished,
                RestartLevel = _restartLevel,
                StartLevel = _startLevel,
                LevelIndex = _levelIndex,
                CollectSaveData = _collectSaveData,
                GameSaveData = _gameSaveData.Value,
                SaveGameTrigger = _saveGameTrigger,
                MoneyAmount = _moneyAmount,
            };
            AddForDisposal(new GamePM(ctx));
        }

        private void OnStartLevel(LevelConfig config)
        {
            RemoveCurrentLevel();
            CreateLevel(config);
        }

        private void CreateLevel(LevelConfig config)
        {
            var ctx = new LevelEntity.Ctx
            {
                ContentProvider = _ctx.ContentProvider,
                UiRoot = _ctx.UiRoot,
                LevelConfig = config,
                LevelCompleted = _levelFinished,
                RestartLevel = _restartLevel,
                LevelIndex = _levelIndex.Value,
                MoneyAmount = _moneyAmount,
            };
            _currentLevelEntity = AddForDisposal(new LevelEntity(ctx));
        }

        private void RemoveCurrentLevel() => 
            _currentLevelEntity?.Dispose();
    }
}