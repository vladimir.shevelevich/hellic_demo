﻿namespace App.Game.Enums
{
    public enum EnemyType
    {
        Footman,
        Jeep,
        Tank,
        Tower,
        BarrelProps,
        TowerProps,
        SmallBuildingProps,
        MediumBuildingProps
    }

    public enum AttackPriorityGroup
    {
        None,
        Barrel,
        Enemy,
        HostageBuilding,
        MoneyKeeper,
        DestroyableProp
    }

    public enum MoneyKeeperType
    {
        SmallBox,
        BigBox
    }

    public enum EnemyAttackType
    {
        Single,
        Double,
        Triple
    }
}