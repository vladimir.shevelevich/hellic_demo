﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace App.Scripts.EditorTools
{
    public static class EditorMenu
    {
        [MenuItem("Helicopter/Clean Save")]
        public static void CleanSave()
        {
            FileUtil.DeleteFileOrDirectory(Application.persistentDataPath);
        }

        [MenuItem("Helicopter/Play Main Scene", false, 0)]
        public static void PlayerMainScene()
        {
            var mainScenePath = EditorBuildSettings.scenes[0].path;
            var mainSceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(mainScenePath);

            EditorSceneManager.playModeStartScene = mainSceneAsset;
            EditorApplication.EnterPlaymode();
        }
    }
}
#endif