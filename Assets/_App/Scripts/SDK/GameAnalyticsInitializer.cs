﻿using GameAnalyticsSDK;
using UnityEngine;

namespace _App.Scripts.SDK
{
    public class GameAnalyticsInitializer : MonoBehaviour
    {
        private void Start()
        {
            GameAnalytics.Initialize();
        }
    }
}