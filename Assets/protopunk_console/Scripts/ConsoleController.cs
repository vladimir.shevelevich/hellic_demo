﻿using System;
using LunarConsolePlugin;
using UnityEditor;
using UnityEngine;

namespace Protopunk_Console.Scripts
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ConsoleController))]
    public class ConsoleControllerVewEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var consoleController = target as ConsoleController;
            if (GUILayout.Button("SEND"))
                consoleController.SendEditorCommand();
        }
    }
#endif

    [CVarContainer]
    public static class ConsoleVariablesData
    {
        public static readonly CVar command = new CVar("command", "");
    }

    public class ConsoleController : MonoBehaviour
    {
        [TextArea] [SerializeField] private string InputArea;

        public static event Action<string> CommandReceived;

        private void Awake()
        {
            ConsoleVariablesData.command.AddDelegate(OnConsoleCommand);
            DontDestroyOnLoad(this);
        }

        public void SendEditorCommand()
        {
            SendCommand(InputArea);
        }

        private void OnConsoleCommand(CVar cVar)
        {
            if (string.IsNullOrEmpty(cVar.Value))
                return;

            SendCommand(cVar.Value);
        }

        private void SendCommand(string command)
        {
            Debug.Log($"Sending console command [{command}]");
            CommandReceived?.Invoke(command);
        }

        private void OnDestroy()
        {
            ConsoleVariablesData.command.RemoveDelegate(OnConsoleCommand);
        }
    }
}