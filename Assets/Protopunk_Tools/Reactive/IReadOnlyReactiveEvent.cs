using System;

namespace Protopunk.Tools
{
    public interface IReadOnlyReactiveEvent<T>
    {
        IDisposable Subscribe(Action<T> action);
        IDisposable SubscribeWithSkip(Action<T> action);
    }

    public interface IReadOnlyReactiveEvent<T1,T2>
    {
        IDisposable Subscribe(Action<T1, T2> action);
        IDisposable SubscribeWithSkip(Action<T1, T2> action);
    }
}