﻿using System;
using UniRx;

namespace Protopunk.Tools
{
    public class ReactiveTrigger : IDisposable, IReadOnlyReactiveTrigger
    {
        private readonly Subject<bool> _subject;

        public ReactiveTrigger()
        {
            _subject = new Subject<bool>();
        }

        public IDisposable Subscribe(Action action)
        {
            return _subject.Subscribe(b => action?.Invoke());
        }

        public void Invoke()
        {
            _subject.OnNext(true);
        }

        public void Dispose()
        {
            _subject.Dispose();
        }
    }
}