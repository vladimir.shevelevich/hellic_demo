﻿using System;

namespace Protopunk.Tools
{
    public interface IReadOnlyReactiveTrigger
    {
        IDisposable Subscribe(Action action);
    }
}