﻿using System;
using UniRx;

namespace Protopunk.Tools
{
    public class ReactiveEvent<T> : IDisposable, IReadOnlyReactiveEvent<T>
    {
        private readonly ReactiveProperty<T> _property;

        public ReactiveEvent()
        {
            _property = new ReactiveProperty<T>();
        }

        public IDisposable Subscribe(Action<T> action)
        {
            return _property.Subscribe(action);
        }

        public IDisposable SubscribeWithSkip(Action<T> action)
        {
            return _property.SkipLatestValueOnSubscribe().Subscribe(action);
        }

        public void Invoke(T obj)
        {
            _property.SetValueAndForceNotify(obj);
        }

        public void Dispose()
        {
            _property?.Dispose();
        }
    }

    public class ReactiveEvent<T1, T2> : IReadOnlyReactiveEvent<T1, T2>, IDisposable
    {
        private struct Entry
        {
            public T1 arg1;
            public T2 arg2;
        }

        private readonly ReactiveProperty<Entry> _property;

        public ReactiveEvent()
        {
            _property = new ReactiveProperty<Entry>();
        }

        public ReactiveEvent(T1 arg1, T2 arg2)
        {
            _property = new ReactiveProperty<Entry>(new Entry { arg1 = arg1, arg2 = arg2 });
        }

        public void Dispose()
        {
            _property?.Dispose();
        }

        public void Invoke(T1 arg1, T2 arg2)
        {
            _property.SetValueAndForceNotify(new Entry { arg1 = arg1, arg2 = arg2 });
        }

        public IDisposable Subscribe(Action<T1, T2> action)
        {
            return _property.Subscribe(entry => action?.Invoke(entry.arg1, entry.arg2));
        }

        public IDisposable SubscribeWithSkip(Action<T1, T2> action)
        {
            return _property.SkipLatestValueOnSubscribe().
                Subscribe(entry => action?.Invoke(entry.arg1, entry.arg2));
        }
    }
}