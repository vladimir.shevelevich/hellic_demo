﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace Protopunk.Tools
{
    public class BaseDisposable : IDisposable
    {
        private List<IDisposable> _disposables;
        private List<MonoBehaviour> _disposablesMonoBehaviors;
        private bool _isDisposed;

        protected T AddForDisposal<T>(T disposable) where T : IDisposable
        {
            _disposables ??= new List<IDisposable>();

            _disposables.Add(disposable);

            return (T)disposable;
        }

        protected T AddForDestroy<T>(T disposableMonoBehavior) where T : MonoBehaviour
        {
            _disposablesMonoBehaviors ??= new List<MonoBehaviour>();
            _disposablesMonoBehaviors.Add(disposableMonoBehavior);
            return (T)disposableMonoBehavior;
        }

        protected async Task<T> LoadAssetAndTrackAsync<T>(AssetReference assetReference) where T : class
        {
            var operationHandle = Addressables.LoadAssetAsync<T>(assetReference);
            await operationHandle.Task;
            if (operationHandle.Status != AsyncOperationStatus.Succeeded || _isDisposed)
            {
                Addressables.Release(operationHandle);
                return null;
            }

            AddForDisposal(new AddressableDisposer(operationHandle));
            return operationHandle.Result;
        }

        public void Dispose()
        {
            if (_disposables != null)
            {
                foreach (var disposable in _disposables)
                {
                    disposable?.Dispose();
                }
            }

            if (_disposablesMonoBehaviors != null)
            {
                foreach (var disposablesMonoBehavior in _disposablesMonoBehaviors)
                {
                    if (disposablesMonoBehavior != null)
                        Object.Destroy(disposablesMonoBehavior.gameObject);
                }
            }

            _isDisposed = true;
            OnDispose();
        }

        protected virtual void OnDispose()
        {

        }
    }
}