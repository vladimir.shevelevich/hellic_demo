using System.Threading.Tasks;

namespace App.Game
{
    public interface IAsyncInitializer
    {
        Task InitializeAsync();
    }
}