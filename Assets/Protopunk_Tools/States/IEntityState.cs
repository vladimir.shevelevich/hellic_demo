﻿using System;

namespace Protopunk.Tools
{
    public interface IEntityState : IDisposable
    {
        void Init();
    }
}