﻿using System;
using Protopunk.Tools;

namespace Protopunk.Tools
{
    public class EntityStateMachine : BaseDisposable
    {
        private IEntityState _currentState;

        protected void SetState(IEntityState state)
        {
            _currentState?.Dispose();

            if (state == null)
                return;

            _currentState = state;
        }

        protected void InitCurrentState()
        {
            _currentState?.Init();
        }
    }
}