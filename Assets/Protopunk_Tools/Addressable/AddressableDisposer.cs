﻿using System;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Protopunk.Tools
{
    public readonly struct AddressableDisposer : IDisposable
    {
        private readonly AsyncOperationHandle _operationHandle;

        public AddressableDisposer(AsyncOperationHandle operationHandle)
        {
            _operationHandle = operationHandle;
        }

        public void Dispose()
        {
            if (_operationHandle.IsValid())
                Addressables.Release(_operationHandle);
        }
    }
}