﻿using System;
using UnityEngine;
using UnityEditor;

namespace Protopunk_Builder
{
    public class Builder : MonoBehaviour
    {
#if UNITY_CLOUD_BUILD
        public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
        {
            SetBundleVersionCode(manifest);
            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.All;
            Debug.Log($"Target architectures: {PlayerSettings.Android.targetArchitectures}");

            PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
            Debug.Log($"Scripting backend: {PlayerSettings.GetScriptingBackend(BuildTargetGroup.Android)}");

            SetAppVersion();

#if CONSOLE_DISABLED
            LunarConsoleEditorInternal.Installer.DisablePlugin();
#endif
        }

        private static void SetBundleVersionCode(UnityEngine.CloudBuild.BuildManifestObject manifest)
        {
            var buildNumber = manifest.GetValue<int>("buildNumber");
            PlayerSettings.Android.bundleVersionCode = buildNumber;
            Debug.Log($"Build number was set to {buildNumber}");
        }

        private static void SetAppVersion()
        {
            var appVersionVar = Environment.GetEnvironmentVariable("AppVersion");
            if (string.IsNullOrEmpty(appVersionVar))
                return;

            PlayerSettings.bundleVersion = appVersionVar;
        }
#endif
    }
}